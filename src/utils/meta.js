export default function meta(metaTags) {
  const metaObj = {};
  // console.log(metaTags);
  if (!metaTags) {
    return metaObj;
  }
  metaObj.meta = {};
  metaObj.link = {};

  if (metaTags.title) {
    // console.log('title added', metaTags.title)

    metaObj.title = metaTags.title;
    metaObj.meta.ogTitle = {name: 'og:title', content: metaTags.title};
    metaObj.meta.twitterTitle = {
      name: 'twitter:title',
      content: metaTags.title,
    };
  }
  if (metaTags.keywords) {
    metaObj.meta.keywords = {name: 'keywords', content: metaTags.keywords};
  }
  if (metaTags.description) {
    // console.log('desc added')
    metaObj.meta.description = {
      name: 'description',
      content: metaTags.description,
    };
    metaObj.meta.ogDescription = {
      name: 'og:description',
      content: metaTags.description,
    };
    metaObj.meta.twitterDescription = {
      name: 'twitter:description',
      content: metaTags.description,
    };
  }

  if (metaTags.url) {
    // console.log('url added')
    metaObj.meta.ogUrl = {name: 'og:url', content: metaTags.url};
    metaObj.meta.twitterUrl = {name: 'twitter:url', content: metaTags.url};
    metaObj.link.canonical = {rel: 'canonical', href: metaTags.url};
  }
  // console.log('image added')
  metaObj.meta.ogImage = {
    name: 'og:image',
    content: metaTags.image !== '' ? metaTags.image : process.env.APP_HOST +
      '/logo.png',
  };
  metaObj.meta.twitterImage = {
    name: 'twitter:image',
    content: metaTags.image !== '' ? metaTags.image : process.env.APP_HOST +
      '/logo.png',
  };
  metaObj.meta.twitterCard = {
    name: 'twitter:card',
    content: 'summary_large_image',
  };
  metaObj.meta.ogType = {name: 'og:type', content: 'website'};
  if (metaTags.robots !== '' && metaTags.robots !== undefined) {
    metaObj.meta.robots = {name: 'robots', content: metaTags.robots};
  }
  return metaObj;
}
