import { Cookies } from "quasar";
/**
 *
 * Получает cookies по названию ключа
 * @export
 * @param {string} name Название ключа
 * @return {string}
 */
export function getCookie(name) {
  const matches = document.cookie.match(
    new RegExp(
      "(?:^|; )" + name.replace(/([.$?*|{}()[\]\\/+^])/g, "\\$1") + "=([^;]*)"
    )
  );
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

export const confirmDialogParams = {
  title: "Подтвердите действие",
  ok: {
    color: "positive",
    label: "Да",
  },
  cancel: {
    color: "negative",
    label: "Нет",
  },
  persistent: true,
  dark: true,
  transitionShow: "slide-down",
  transitionHide: "slide-down",
  style: "max-width: 100%",
};
/**
 * Проверяет является ли строка json
 *
 * @export
 * @param {string} item Строка для проверки
 * @return {boolean}
 */
export function isJson(item) {
  item = typeof item !== "string" ? JSON.stringify(item) : item;
  try {
    item = JSON.parse(item);
  } catch (e) {
    return false;
  }
  if (typeof item === "object" && item !== null) return true;
  return false;
}

export function itemPriscriboContent(item) {
  if (!isJson(item)) return item;
  else {
    if (JSON.parse(item).content.length > 0) {
      let str = "";
      JSON.parse(item).content.forEach((k) => {
        // if (i === 0) return;
        try {
          const oneString = k.content
            .map((val) => {
              return val.type === "text" ? val.text.trim() : "";
            })
            .join(" ");
          str = str + oneString + "\n";
        } catch (e) {}
      });
      return str;
    } else return "";
  }
}

export function idParser(path, id, number) {
  if (!id && !number) return;

  let idToString = "";
  if (Object.prototype.hasOwnProperty.call(this.$route.params, id)) {
    idToString = this.$route.params[id].toString();
  } else {
    // console.log('no property id' );
    // console.log('number is: ',number);
  }

  const str = id ? idToString : number.toString();
  const matched = str.match(/[0-9]+/);
  if (matched) {
    if (matched[0] === str) return +str;
  }

  if (path) {
    this.$router.push(path);
    requestAnimationFrame(() => this.$store.commit("UIstore/resetIsLoading"));
  }
}

export function yandexBanner() {
  (function (w, d, n, s, t) {
    w[n] = w[n] || [];
    w[n].push(function () {
      Ya.Context.AdvManager.render(
        {
          blockId: "R-A-945396-1",
          renderTo: "yandex_rtb_R-A-945396-1",
          async: true,
          // eslint-disable-next-line no-unused-vars
          onRender: function (data) {
            // console.log(data.product);
          },
        },
        function altCallback() {
          // код вызова своей рекламы в блоке
        }
      );
    });
    t = d.getElementsByTagName("script")[0];
    s = d.createElement("script");
    s.type = "text/javascript";
    s.src = "//an.yandex.ru/system/context.js";
    s.async = true;
    t.parentNode.insertBefore(s, t);
  })(window, document, "yandexContextAsyncCallbacks");
}

export function yandexMetrika() {
  const ymetrikaId = 99999999;
  //******************Метрика Yandex**********************

  (function (m, e, t, r, i, k, a) {
    m[i] =
      m[i] ||
      function () {
        (m[i].a = m[i].a || []).push(arguments);
      };
    m[i].l = 1 * new Date();
    k = e.createElement(t);
    a = e.getElementsByTagName(t)[0];
    k.async = 1;
    k.src = r;
    a.parentNode.insertBefore(k, a);
  })(window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

  ym(ymetrikaId, "init", {
    clickmap: true,
    trackLinks: true,
    accurateTrackBounce: true,
  });

  //******************Метрика Yandex**********************
}
/** Возвращает url бакенда в зависимости от контекста
 * @returns {string}
 */
export function getAPIurl() {
  if (process.env.MODE === "capacitor") {
    return (
      process.env.CAPACITOR_GRAPHQL_URI ||
      process.env.GRAPHQL_URI ||
      "http://localhost:8000/api/v1.1/"
    ).replace(/['"]/g, "");
  } else
    return (
      process.env.GRAPHQL_URI || "http://localhost:8000/api/v1.1/"
    ).replace(/['"]/g, "");
}

import { date } from "quasar";
const { addToDate, getDateBetween, formatDate } = date;
/**
 * Принимает строку даты и проверяет её соответствие реалиям. Если дата не парсится, то устанавливается текущая дата. Если дата ранее текущей, то возвращается текущая. Если дата сильно в будущем, то возвращается дата "сегодня + 11 месяцев"
 * @param {string} dateString Строка даты
 * @returns {string}
 */
export function formatDateString(dateString) {
  let newDate = new Date(dateString);
  const dateMin = new Date();
  if (isNaN(newDate)) {
    newDate = dateMin;
  }
  const dateMax = addToDate(dateMin, { months: 11 });
  const dateNormalized = getDateBetween(newDate, dateMin, dateMax);
  const formattedString = formatDate(dateNormalized, "YYYY-MM-DD");
  return formattedString;
}

import fetch from "cross-fetch";
/**
 * Ищет соответствия строки в ФИАС
 * @async
 * @export
 * @param {string} query Строка для поиска
 * @return {Promise<string>}
 */
export async function suggestionsFias(query) {
  const url =
    "https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address";
  const token = "d434a274c6ec1205df554014f5f7c0914e402abf";

  const options = {
    method: "POST",
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: "Token " + token,
    },
    body: JSON.stringify({ query: query }),
  };

  try {
    const response = await fetch(url, options);
    const json = await response.json();
    return Promise.resolve(json);
  } catch (error) {
    console.log("error", error);
    return Promise.reject(error);
  }
}

/**
 * Выводит лог в зависимости от установки переменной окружения DEBUG
 *
 * @export
 * @param {string} item строка для вывода в лог
 */
export function debugLog(...items) {
  if (process.env.DEBUG === true || process.env.DEBUG === "true") {
    console.log(...items);
  }
}
