const words = {
  "/": "main",
};
//целевые страницы
const targetPath = ["main"];

/*
 * goal: [submitted, click]
 * target: [button1, button2, popup1, popup2, form, callback, whatsapp, telegram, facebook, email, instagram, vk, ]
 * */

export function sendYMgoal(path, goal, target) {
  let str = "";
  if (words[path] === undefined || words[path] === "") {
    str += "unknown";
  } else str += words[path];
  str = `${str}_${goal}_${target}`;

  if (targetPath.includes(words[path]) || target === "callback") {
    ym(89946817, "reachGoal", str);
    ym(89982038, "reachGoal", str);
  }
}
