const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "",
        component: () => import("pages/IndexPage.vue"),
        meta: { pageName: "Знания", requiresAuth: false },
      },
      {
        path: "/actions",
        component: () => import("pages/ActionsPage.vue"),
        meta: { pageName: "Действия", requiresAuth: false },
      },
      {
        path: "/universes",
        component: () => import("pages/UniversesPage.vue"),
        meta: { pageName: "Вселенные", requiresAuth: false },
      },
      {
        path: "/profile",
        component: () => import("pages/UserProfile.vue"),
        meta: { pageName: "Профиль", requiresAuth: true },
      },
      {
        path: "/changelog",
        component: () => import("pages/ChangeLogPage.vue"),
        meta: { pageName: "История изменений", requiresAuth: false },
      },
      {
        path: "/roadmap",
        component: () => import("pages/RoadMapPage.vue"),
        meta: { pageName: "Дорожная карта", requiresAuth: false },
      },
      {
        path: "/ol",
        component: () => import("pages/OmskLicensePage.vue"),
        meta: { pageName: "Омская лицензия", requiresAuth: false },
      },
      {
        path: "/premio",
        component: () => import("pages/AwardsPage.vue"),
        meta: { pageName: "Награды", requiresAuth: false },
      },
      { path: "/n/o:award_number", redirect: "/premio" },
      {
        path: "/hyperuniverse",
        component: () => import("pages/HyperUniverse.vue"),
        meta: { pageName: "Гипервселенная", requiresAuth: false },
      },
      {
        path: "/howcanwehelp",
        component: () => import("pages/HowCanWeHelp.vue"),
        meta: { pageName: "Как нам помочь?", requiresAuth: false },
      },
      {
        path: "/nft",
        component: () => import("pages/OurNft.vue"),
        meta: { pageName: "NFT", requiresAuth: false },
      },
      {
        path: "/privacy_policy",
        component: () => import("pages/privacyPolicy.vue"),
        meta: { pageName: "Политика конфиденциальности", requiresAuth: false },
      },
      {
        path: "/restarigo",
        component: () => import("pages/restarigo"),
        meta: { pageName: "Восстановление доступа", requiresAuth: false },
      },
      {
        path: "/registrado",
        component: () => import("pages/registrado"),
        meta: { pageName: "Регистрация", requiresAuth: false },
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;
