import gql from "graphql-tag";

//отправка запроса на почту
export const sendMail = gql`
  mutation sendQueryToMail(
    $nomo: String!
    $telefono: String
    $organizo: String
    $retposhto: String
  ) {
    enketo(
      nomo: $nomo
      telefono: $telefono
      organizo: $organizo
      retposhto: $retposhto
    ) {
      status
      message
    }
  }
`;
//добавление проекта
export const addProjekto = gql`
  mutation addProjekto(
    $publikigo: Boolean = true
    $nomo: String!
    $kategorio: [Int] = 1
    $tipoId: Int = 1
    $statusoId: Int = 1
    $priskribo: String!
    $posedantoUzantoId: Int!
    $posedantoKomunumoId: Int!
    $posedantoTipoId: Int = 1
    $posedantoStatusoId: Int = 1
    $uuid: UUID
  ) {
    redaktuUniversoProjektojProjekto(
      publikigo: $publikigo
      nomo: $nomo
      kategorio: $kategorio
      tipoId: $tipoId
      statusoId: $statusoId
      priskribo: $priskribo
      posedantoUzantoId: $posedantoUzantoId
      posedantoKomunumoId: $posedantoKomunumoId
      posedantoTipoId: $posedantoTipoId
      posedantoStatusoId: $posedantoStatusoId
      uuid: $uuid
    ) {
      status
      message
      universoProjekto {
        uuid
        id
        objId
        nomo {
          enhavo
        }
        priskribo {
          enhavo
        }
        tipo {
          nomo {
            enhavo
          }
        }
        pozicio
        realeco {
          nomo {
            enhavo
          }
        }
        kategorio {
          edges {
            node {
              nomo {
                enhavo
              }
            }
          }
        }
        statuso {
          nomo {
            enhavo
          }
        }
        objekto {
          nomo {
            enhavo
          }
        }
        universoprojektojprojektoposedantoSet {
          edges {
            node {
              projekto {
                nomo {
                  enhavo
                }
              }
            }
          }
        }
        tasko {
          edges {
            node {
              id
              nomo {
                enhavo
              }
              priskribo {
                enhavo
              }
            }
          }
        }
      }
    }
  }
`;
//регистрация пользователя Универсо
export const addUniversoUzanto = gql`
  mutation ($retnomo: String!, $uuid: UUID) {
    redaktuProfilo(retnomo: $retnomo, uuid: $uuid, publikigo: true) {
      status
      message
    }
  }
`;
//удаление проекта
export const forigoProjekto = gql`
  mutation forigoProjekto($id: UUID, $forigo: Boolean = true) {
    redaktuUniversoProjektojProjekto(uuid: $id, forigo: $forigo) {
      status
      message
    }
  }
`;
//архивация проекта
export const arkProjekto = gql`
  mutation arkProjekto($id: UUID, $arkivo: Boolean = true) {
    redaktuUniversoProjektojProjekto(uuid: $id, arkivo: $arkivo) {
      status
      message
    }
  }
`;
//добавление задачи
export const addTasko = gql`
  mutation addTasko(
    $publikigo: Boolean = true
    $nomo: String!
    $kategorio: [Int] = 1
    $tipoId: Int = 1
    $statusoId: Int = 1
    $priskribo: String!
    $projektoUuid: String
  ) {
    redaktuUniversoProjektojTaskoj(
      publikigo: $publikigo
      nomo: $nomo
      kategorio: $kategorio
      tipoId: $tipoId
      statusoId: $statusoId
      priskribo: $priskribo
      projektoUuid: $projektoUuid
    ) {
      status
      message
      universoTaskoj {
        uuid
        id
        objId
        nomo {
          enhavo
        }
        priskribo {
          enhavo
        }
        kategorio {
          edges {
            node {
              objId
              nomo {
                enhavo
              }
            }
          }
        }
        tipo {
          objId
          nomo {
            enhavo
          }
        }
        statuso {
          objId
          nomo {
            enhavo
          }
        }
      }
    }
  }
`;
//создание категории задач
export const addTaskojKategorio = gql`
  mutation addTaskojKategorio(
    $publikigo: Boolean = true
    $nomo: String!
    $realeco: [Int] = 1
  ) {
    redaktuUniversoProjektojTaskojKategorio(
      publikigo: $publikigo
      nomo: $nomo
      realeco: $realeco
    ) {
      status
      message
      universoTaskojKategorioj {
        uuid
        id
        objId
      }
    }
  }
`;
//создание типов задач
export const addTaskojTipo = gql`
  mutation addTaskojTipo(
    $publikigo: Boolean = true
    $nomo: String!
    $realeco: [Int] = 1
  ) {
    redaktuUniversoProjektojTaskojTipo(
      publikigo: $publikigo
      nomo: $nomo
      realeco: $realeco
    ) {
      status
      message
      universoTaskojTipoj {
        uuid
        id
        objId
      }
    }
  }
`;
//создание статусов задач
export const addTaskojStatuso = gql`
  mutation addTaskojStatuso($publikigo: Boolean = true, $nomo: String!) {
    redaktuUniversoProjektojTaskoStatuso(publikigo: $publikigo, nomo: $nomo) {
      status
      message
      universoTaskojStatusoj {
        uuid
        id
        objId
      }
    }
  }
`;
//архивация задачи
export const arkTasko = gql`
  mutation arkTasko($id: UUID, $arkivo: Boolean = true) {
    redaktuUniversoProjektojTaskoj(uuid: $id, arkivo: $arkivo) {
      status
      message
    }
  }
`;
//удаление задачи
export const forigoTasko = gql`
  mutation forigoTasko($id: UUID, $forigo: Boolean = true) {
    redaktuUniversoProjektojTaskoj(uuid: $id, forigo: $forigo) {
      status
      message
    }
  }
`;
//логин
export const login_query = gql`
  mutation ($login: String!, $password: String!) {
    ensaluti(login: $login, password: $password) {
      status
      message
      konfirmita
      token
      csrfToken
      jwtToken
    }
  }
`;
//логаут
export const logout_query = gql`
  mutation {
    elsaluti {
      status
      message
    }
  }
`;
//изменение пароля
export const changePassword = gql`
  mutation shanghiPasvorton(
    $uzantoId: Int
    $pasvorto: String!
    $novaPasvorto: String!
  ) {
    shanghiPasvorton(
      uzantoId: $uzantoId
      pasvorto: $pasvorto
      novaPasvorto: $novaPasvorto
    ) {
      status
      message
      csrfToken
    }
  }
`;
//изменение настроек профиля пользователя
export const changeSettings = gql`
  mutation aplikiAgordoj(
    $avataro: String
    $chefaOrganizo: Int
    $duaNomo: String
    $familinomo: String
    $id: Int
    $kovrilo: String
    $lando: String
    $lingvo: String
    $naskighdato: Date
    $poshtoSciigoj: Boolean
    $regiono: String
    $sekso: String
    $statuso: String
    $unuaNomo: String
  ) {
    aplikiAgordoj(
      unuaNomo: $unuaNomo
      duaNomo: $duaNomo
      familinomo: $familinomo
      avataro: $avataro
      chefaOrganizo: $chefaOrganizo
      kovrilo: $kovrilo
      id: $id
      lando: $lando
      lingvo: $lingvo
      naskighdato: $naskighdato
      poshtoSciigoj: $poshtoSciigoj
      regiono: $regiono
      sekso: $sekso
      statuso: $statuso
    ) {
      status
      message
      errors {
        field
        message
      }
      poshtoSciigoj
      unuaNomo {
        lingvo
        enhavo
        chefaVarianto
      }
      duaNomo {
        lingvo
        enhavo
        chefaVarianto
      }
      familinomo {
        lingvo
        enhavo
        chefaVarianto
      }
      sekso
      statuso {
        lingvo
        enhavo
        chefaVarianto
      }
      naskighdato
      lingvo {
        id
      }
      lando {
        id
      }
      regiono {
        id
      }
      chefaOrganizo {
        id
      }
      avataro {
        id
      }
      kovrilo {
        id
      }
    }
  }
`;
//изменение проекта
export const changeProjekto = gql`
  mutation changeProjekto(
    $publikigo: Boolean
    $nomo: String
    $kategorio: [Int]
    $tipoId: Int
    $statusoId: Int
    $priskribo: String
    $posedantoUzantoId: Int
    $posedantoKomunumoId: Int
    $posedantoTipoId: Int
    $posedantoStatusoId: Int
    $uuid: UUID!
  ) {
    redaktuUniversoProjektojProjekto(
      publikigo: $publikigo
      nomo: $nomo
      kategorio: $kategorio
      tipoId: $tipoId
      statusoId: $statusoId
      priskribo: $priskribo
      posedantoUzantoId: $posedantoUzantoId
      posedantoKomunumoId: $posedantoKomunumoId
      posedantoTipoId: $posedantoTipoId
      posedantoStatusoId: $posedantoStatusoId
      uuid: $uuid
    ) {
      status
      message
      universoProjekto {
        uuid
        id
        objId
        nomo {
          enhavo
        }
        priskribo {
          enhavo
        }
        tipo {
          nomo {
            enhavo
          }
        }
        pozicio
        realeco {
          nomo {
            enhavo
          }
        }
        kategorio {
          edges {
            node {
              nomo {
                enhavo
              }
            }
          }
        }
        statuso {
          nomo {
            enhavo
          }
        }
        objekto {
          nomo {
            enhavo
          }
        }
        universoprojektojprojektoposedantoSet {
          edges {
            node {
              projekto {
                nomo {
                  enhavo
                }
              }
            }
          }
        }
        tasko {
          edges {
            node {
              uuid
              id
              objId
              nomo {
                enhavo
              }
              priskribo {
                enhavo
              }
              kategorio {
                edges {
                  node {
                    objId
                    nomo {
                      enhavo
                    }
                  }
                }
              }
              tipo {
                objId
                nomo {
                  enhavo
                }
              }
              statuso {
                objId
                nomo {
                  enhavo
                }
              }
            }
          }
        }
      }
    }
  }
`;
//изменение задачи
export const changeTasko = gql`
  mutation changeTasko(
    $publikigo: Boolean
    $nomo: String
    $kategorio: [Int]
    $tipoId: Int
    $statusoId: Int
    $priskribo: String
    $posedantoUzantoId: Int
    $posedantoTipoId: Int
    $posedantoStatusoId: Int
    $uuid: UUID!
  ) {
    redaktuUniversoProjektojTaskoj(
      publikigo: $publikigo
      nomo: $nomo
      kategorio: $kategorio
      tipoId: $tipoId
      statusoId: $statusoId
      priskribo: $priskribo
      posedantoUzantoId: $posedantoUzantoId
      posedantoTipoId: $posedantoTipoId
      posedantoStatusoId: $posedantoStatusoId
      uuid: $uuid
    ) {
      status
      message
      universoTaskoj {
        objId
        nomo {
          enhavo
        }
        priskribo {
          enhavo
        }
        uuid
        kategorio {
          edges {
            node {
              objId
              id
              nomo {
                enhavo
              }
            }
          }
        }
        statuso {
          objId
          id
          nomo {
            enhavo
          }
        }
        tipo {
          objId
          id
          nomo {
            enhavo
          }
        }
        forigo
        forigaDato
        arkivo
        arkivaDato
      }
    }
  }
`;
//изменение телефона или электронной почты
export const changeTelephoneNumberOrEmail = gql`
  mutation shanghiPoshtonTelefonon(
    $tel: String
    $code: String
    $email: String
  ) {
    shanghiPoshtonTelefonon(
      telefonaNombro: $tel
      konfirmaKodo: $code
      retposhto: $email
    ) {
      status
      message
    }
  }
`;
//изменение аватары
export const changeAvatar = gql`
  mutation instaliAvataron(
    $id: Int!
    $baza: String
    $full: String
    $min: String
  ) {
    instaliAvataron(
      uzantoId: $id
      bildoBaza: $baza
      bildoAvataro: $full
      bildoAvataroMin: $min
    ) {
      status
      message
      errors {
        field
        message
      }
      avataro {
        bildoE {
          url
        }
        bildoF {
          url
        }
      }
    }
  }
`;
//изменение или создание связей проектов
export const changeLigilo = gql`
  mutation changeLigilo(
    $uuid: UUID
    $tipoId: Int
    $arkivo: Boolean
    $forigo: Boolean
    $ligiloId: Int
    $posedantoId: Int
    $publikigo: Boolean
  ) {
    redaktuUniversoProjektojProjektoLigilo(
      uuid: $uuid
      tipoId: $tipoId
      arkivo: $arkivo
      forigo: $forigo
      ligiloId: $ligiloId
      posedantoId: $posedantoId
      publikigo: $publikigo
    ) {
      status
      message
      universoProjektoLigilo {
        uuid
        posedanto {
          objId
          id
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
        }
        ligilo {
          objId
          id
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
        }
        tipo {
          id
          uuid
          objId
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
        }
        id
      }
    }
  }
`;
//вступление в сообщество
export const kuniguKomunumon = gql`
  mutation kuniguKomunumon($komunumoUuid: UUID!) {
    kuniguKomunumon(komunumoUuid: $komunumoUuid) {
      status
      message
      statistiko {
        mia
      }
    }
  }
`;
//выход из сообщества
export const forlasuKomunumon = gql`
  mutation forlasuKomunumon($komunumoUuid: UUID!) {
    forlasuKomunumon(komunumoUuid: $komunumoUuid) {
      status
      message
      statistiko {
        mia
      }
    }
  }
`;

// Создание категорий документов:
export const redaktuDokumentoKategorio = gql`
  mutation DokumentoKategorio(
    $nomo: String
    $priskribo: String
    $realeco: [Int] = [1, 2, 3]
    $publikigo: Boolean = true
  ) {
    redaktuUniversoDokumentoKategorio(
      nomo: $nomo
      priskribo: $priskribo
      realeco: $realeco
      publikigo: $publikigo
    ) {
      status
      message
      universoDokumentojKategorioj {
        uuid
      }
    }
  }
`;

// Типы связей категорий документов между собой
export const redaktuDokumentoKategorioLigilojTipo = gql`
  mutation redaktuDokumentoKategorioLigilojTipo(
    $nomo: String
    $priskribo: String
    $publikigo: Boolean = true
  ) {
    redaktuUniversoDokumentoKategorioLigilojTipo(
      nomo: $nomo
      priskribo: $priskribo
      publikigo: $publikigo
    ) {
      status
      message
      universoDokumentojKategoriojDokumentojLigilojTipoj {
        uuid
      }
    }
  }
`;

// модель связей категорий документов между собой
export const redaktuDokumentoKategorioLigiloj = gql`
  mutation redaktuDokumentoKategorioLigiloj(
    $posedantoId: Int
    $ligiloId: Int
    $tipoId: Int
    $publikigo: Boolean = true
  ) {
    redaktuUniversoDokumentoKategorioLigiloj(
      posedantoId: $posedantoId
      ligiloId: $ligiloId
      tipoId: $tipoId
      publikigo: $publikigo
    ) {
      status
      message
      universoDokumentojKategoriojDokumentojLigiloj {
        uuid
      }
    }
  }
`;
// Типы документов в Универсо
export const redaktuDokumentoTipo = gql`
  mutation redaktuDokumentoTipo(
    $nomo: String
    $priskribo: String
    $realeco: [Int] = [1, 2, 3]
    $publikigo: Boolean = true
  ) {
    redaktuUniversoDokumentoTipo(
      nomo: $nomo
      priskribo: $priskribo
      realeco: $realeco
      publikigo: $publikigo
    ) {
      status
      message
      universoDokumentojTipoj {
        uuid
      }
    }
  }
`;

// Виды документов
export const redaktuDokumentoSpeco = gql`
  mutation redaktuDokumentoSpeco(
    $nomo: String
    $priskribo: String
    $realeco: [Int] = [1, 2, 3]
    $publikigo: Boolean = true
  ) {
    redaktuUniversoDokumentoSpeco(
      nomo: $nomo
      priskribo: $priskribo
      realeco: $realeco
      publikigo: $publikigo
    ) {
      status
      message
      universoDokumentojSpecoj {
        uuid
      }
    }
  }
`;

// Справочник документов

export const redaktuDokumento = gql`
  mutation redaktuDokumento(
    $nomo: String
    $priskribo: String
    $realeco: [Int] = [1, 2, 3]
    $kategorio: [Int] = [1, 2]
    $tipoId: Int = 1
    $specoId: Int = 1
    $publikigo: Boolean = true
    $uuid: UUID
    $forigo: Boolean = false
    $arkivo: Boolean = false
  ) {
    redaktuUniversoDokumento(
      nomo: $nomo
      priskribo: $priskribo
      realeco: $realeco
      kategorio: $kategorio
      tipoId: $tipoId
      specoId: $specoId
      publikigo: $publikigo
      forigo: $forigo
      arkivo: $arkivo
      uuid: $uuid
    ) {
      status
      message
      universoDokumentoj {
        uuid
        id
        objId
        nomo {
          enhavo
        }
        priskribo {
          enhavo
        }
      }
    }
  }
`;

// Типы мест хранения документов

export const redaktuDokumentoStokejojTipo = gql`
  mutation redaktuDokumentoStokejojTipo(
    $nomo: String
    $priskribo: String
    $publikigo: Boolean = true
  ) {
    redaktuUniversoDokumentoStokejojTipo(
      nomo: $nomo
      priskribo: $priskribo
      publikigo: $publikigo
    ) {
      status
      message
      universoDokumentojStokejojTipoj {
        uuid
      }
    }
  }
`;

// логические места хранения документов

export const redaktuDokumentoStokejoj = gql`
  mutation redaktuDokumentoStokejoj(
    $posedantoId: Int = 1
    $tipoId: Int = 1
    $nomo: String
    $priskribo: String
    $publikigo: Boolean = true
  ) {
    redaktuUniversoDokumentoStokejoj(
      posedantoId: $posedantoId
      tipoId: $tipoId
      nomo: $nomo
      priskribo: $priskribo
      publikigo: $publikigo
    ) {
      status
      message
      universoDokumentojStokejoj {
        uuid
      }
    }
  }
`;

// Типы владельцев документов
export const redaktuDokumentoPosedantoTipo = gql`
  mutation redaktuDokumentoPosedantoTipo(
    $nomo: String
    $priskribo: String
    $publikigo: Boolean = true
  ) {
    redaktuUniversoDokumentoPosedantoTipo(
      nomo: $nomo
      priskribo: $priskribo
      publikigo: $publikigo
    ) {
      status
      message
      universoDokumentojPosedantojTipoj {
        uuid
      }
    }
  }
`;

// Статус владельца документа
export const redaktuDokumentoPosedantoStatuso = gql`
  mutation redaktuDokumentoPosedantoStatuso(
    $nomo: String
    $priskribo: String
    $publikigo: Boolean = true
  ) {
    redaktuUniversoDokumentoPosedantoStatuso(
      nomo: $nomo
      priskribo: $priskribo
      publikigo: $publikigo
    ) {
      status
      message
      universoDokumentojPosedantojStatusoj {
        uuid
      }
    }
  }
`;

// Владельцы документов

export const redaktuDokumentoPosedantoj = gql`
  mutation redaktuDokumentoPosedantoj(
    $realecoId: Int = 1
    $dokumentoId: Int = 1
    $tipoId: Int = 1
    $statusoId: Int = 1
    $parto: Int = 65
    $publikigo: Boolean = true
  ) {
    redaktuUniversoDokumentoPosedantoj(
      realecoId: $realecoId
      dokumentoId: $dokumentoId
      tipoId: $tipoId
      statusoId: $statusoId
      parto: $parto
      publikigo: $publikigo
    ) {
      status
      message
      universoDokumentojPosedantoj {
        uuid
      }
    }
  }
`;

// Типы связей документов между собой

export const redaktuDokumentoLigilojTipo = gql`
  mutation redaktuDokumentoLigilojTipo(
    $nomo: String
    $priskribo: String
    $publikigo: Boolean = true
  ) {
    redaktuUniversoDokumentoLigilojTipo(
      nomo: $nomo
      priskribo: $priskribo
      publikigo: $publikigo
    ) {
      status
      message
      universoDokumentojLigilojTipoj {
        uuid
      }
    }
  }
`;

// Связь документов между собой
export const redaktuDokumentoLigiloj = gql`
  mutation redaktuDokumentoLigiloj(
    $priskribo: String
    $posedantoId: Int = 1
    $ligiloId: Int = 1
    $tipoId: Int = 1
    $konektilo: Int = 1
    $publikigo: Boolean = true
  ) {
    redaktuUniversoDokumentoLigiloj(
      priskribo: $priskribo
      posedantoId: $posedantoId
      ligiloId: $ligiloId
      tipoId: $tipoId
      konektilo: $konektilo
      publikigo: $publikigo
    ) {
      status
      message
      universoDokumentojLigiloj {
        uuid
      }
    }
  }
`;
// добавление пользователя в организацию
export const addUserToCompany = gql`
  mutation addUserToCompany(
    $publikigo: Boolean = true
    $organizoUuid: UUID
    $uzantoId: Int
  ) {
    redaktuOrganizoMembro(
      publikigo: $publikigo
      organizoUuid: $organizoUuid
      uzantoId: $uzantoId
    ) {
      status
      message
      organizojMembroj {
        uuid
      }
    }
  }
`;
// отправка заявки
export const addRequest = gql`
  mutation aldoniDokumentoEkspedo(
    $klientoUuid: String #  // uuid Заказчик
    $rendevuejo: Boolean # = false  //Сборный груз
    $telefonoKliento: String # = ""  //Телефон заказчика
    $volumeno: Float # = 1  //объём
    $pezo: Int # = 1  //вес
    $priskriboKargo: String # = ""  // Описание груза
    $kostoKargo: Float # = 1  //Стоимость груза
    $monsumoKliento: Float # = 1  //Сумма заказчика
    $monsumoTransportisto: Float # = 0  //Сумма перевозчика
    $adresoKargado: String # = ""  //Адрес загрузки
    $adresoElsxargxado: String # = ""  //Адрес выгрузки
    $tempoS: DateTime # = "2022-01-20T08:53:36"  //Время с (про склад???)
    $tempoElsxargxado: DateTime # = "2022-01-20T08:53:36"  // Время выгрузки с
    $konservejoRicevadoDato: Date # = "2022-01-20"  //СКЛ дата приемки
    $konservejoRicevadoTempoDe: Time # = "08:53:36"  //СКЛ время приемки с
    $konservejoRicevadoTempoEn: Time # = "08:53:36"  //СКЛ время приемки по
    $konservejoFordonoDato: Date # = "2022-01-20"  //СКЛ дата передачи
    $konservejoFordonoTempoDe: Time # = "08:53:36"  //СКЛ время передачи с
    $konservejoFordonoTempoEn: Time # = "08:53:36"  //СКЛ время передачи по
    $organizoAdreso: String # = ""  //Организация по адресу
    $organizoAdresoElsxargxado: String # = ""  //Организация по адресу разгрузки
    $kombatantoAdresoj: String # = ""  //Контактное лицо по загрузке
    $kombatantoAdresojElsxargxado: String # = ""  //Контактное лицо по разгрузки
    $kombatantoKlientoUuid: String #  //Контактное лицо заказчика
    $urbojRecivadoNomo: String # = "Москва"  // Город приемки
    $organizoUuid: String #  //uuid обслуживающей организации
    $urbojTransdonoNomo: String # = "Кострома"  //Город передачи
    $valutoKlientoId: Int # = 840  // Валюта заказчика
    $klientoSciigi: Boolean # = true  // Признак: заказчик извещен
    $projektoStatusoId: Int = 7 # //Статус проекта
    $taskoNumero: [Int] # = [1]  // Порядковый номер задач
    $taskoKomDato: [DateTime] # = ["2022-01-20T08:53:36"]  //Дата начала выполнения задач
    $taskoFinDato: [DateTime] # = ["2022-01-20T08:53:36"]  //Дата окончания выполнения задач
    $taskoKomAdreso: [String] # = ["стартовый адрес"]  //Адрес начала выполнения задач
    $taskoFinAdreso: [String] # = ["финальный адрес"]  //Адрес окончания выполнения задач
    $taskoStatusoId: [Int] # = [7]  //Статус задач
    $taskoPriskribo: [String] # = ["описание задачи"]  // Комментарии задач
    $taskoNomo: [String] # = [" название задачи"]  // Название задач
    $kargoNumero: [Int] # = [1]  // Порядковый номер
    $kargoNomo: [String] # = ["наименование"]  // Наименование
    $kargoKvantoPecoj: [Int] # = [2]  // Количество мест
    $kargoLongo: [Float] # = [1.2]  // Длина
    $kargoLargho: [Float] # = [2.34]  //Ширина
    $kargoAlto: [Float] # = [0.34]  // Высота
    $kargoPezoFakta: [Float] # = [24.234]  // Вес фактический
    $kargoVolumeno: [Float] # = [4.345]  // Объем
    $kargoPezoVolumena: [Float] # = [21.124]  // Вес объемный
    $kargoTipoPakumojUuid: [String] # = ["fbad3a84-3c4b-11e6-80fc-000c298958d9"]  // UUID типа упаковки
    $kargoIndikatoro: [Boolean] # = [true]  // Нужен ли датчик
    $kargoNumeroIndikatoro: [String] # = ["134rge5"]  //Серийный номер датчика
    $kargoTemperaturaReghimoUuid: [String] # = ["f96480f0-8f89-11ea-8ba1-005056a9edf3"]  // UUID терморежима
    $kargoGrave: [Boolean] # = [true]  //Опасный/Неопасный
    $kargoRetropasxoPakumo: [Boolean] # = [true]  //Возврат упаковки
    $kargoRetropasxoIndikatoro: [Boolean] # = [true]  //Возврат датчика
    $priskriboDangxeraKargo: String #Описание опасного груза
    $klasoDangxera: String # Класс опасности
    $ricevantoUuid: String #Грузополучатель
    $kombatantoKontraktoUuid: String #//???контактное лицо получателя ????
    $telefonoAdresoElsxargxado: String #//телефон по адресу разгрузки
    $telefonoAdreso: String
  ) {
    aldoniDokumentoEkspedo(
      klientoUuid: $klientoUuid
      rendevuejo: $rendevuejo
      telefonoKliento: $telefonoKliento
      volumeno: $volumeno
      pezo: $pezo
      priskriboKargo: $priskriboKargo
      kostoKargo: $kostoKargo
      monsumoKliento: $monsumoKliento
      monsumoTransportisto: $monsumoTransportisto
      adresoKargado: $adresoKargado
      adresoElsxargxado: $adresoElsxargxado
      tempoS: $tempoS
      tempoElsxargxado: $tempoElsxargxado
      konservejoRicevadoDato: $konservejoRicevadoDato
      konservejoRicevadoTempoDe: $konservejoRicevadoTempoDe
      konservejoRicevadoTempoEn: $konservejoRicevadoTempoEn
      konservejoFordonoDato: $konservejoFordonoDato
      konservejoFordonoTempoDe: $konservejoFordonoTempoDe
      konservejoFordonoTempoEn: $konservejoFordonoTempoEn
      organizoAdreso: $organizoAdreso
      organizoAdresoElsxargxado: $organizoAdresoElsxargxado
      kombatantoAdresoj: $kombatantoAdresoj
      kombatantoAdresojElsxargxado: $kombatantoAdresojElsxargxado
      kombatantoKlientoUuid: $kombatantoKlientoUuid
      urbojRecivadoNomo: $urbojRecivadoNomo
      organizoUuid: $organizoUuid
      urbojTransdonoNomo: $urbojTransdonoNomo
      valutoKlientoId: $valutoKlientoId
      klientoSciigi: $klientoSciigi
      projektoStatusoId: $projektoStatusoId
      taskoNumero: $taskoNumero
      taskoKomDato: $taskoKomDato
      taskoFinDato: $taskoFinDato
      taskoKomAdreso: $taskoKomAdreso
      taskoFinAdreso: $taskoFinAdreso
      taskoStatusoId: $taskoStatusoId
      taskoPriskribo: $taskoPriskribo
      taskoNomo: $taskoNomo
      kargoNumero: $kargoNumero
      kargoNomo: $kargoNomo
      kargoKvantoPecoj: $kargoKvantoPecoj
      kargoLongo: $kargoLongo
      kargoLargho: $kargoLargho
      kargoAlto: $kargoAlto
      kargoPezoFakta: $kargoPezoFakta
      kargoVolumeno: $kargoVolumeno
      kargoPezoVolumena: $kargoPezoVolumena
      kargoTipoPakumojUuid: $kargoTipoPakumojUuid
      kargoIndikatoro: $kargoIndikatoro
      kargoNumeroIndikatoro: $kargoNumeroIndikatoro
      kargoTemperaturaReghimoUuid: $kargoTemperaturaReghimoUuid
      kargoGrave: $kargoGrave
      kargoRetropasxoPakumo: $kargoRetropasxoPakumo
      kargoRetropasxoIndikatoro: $kargoRetropasxoIndikatoro
      priskriboDangxeraKargo: $priskriboDangxeraKargo
      klasoDangxera: $klasoDangxera
      ricevantoUuid: $ricevantoUuid
      kombatantoKontraktoUuid: $kombatantoKontraktoUuid
      telefonoAdresoElsxargxado: $telefonoAdresoElsxargxado
      telefonoAdreso: $telefonoAdreso
    ) {
      status
      message
      errors {
        field
        message
      }
      ekspedo {
        uuid
        kodo
      }
    }
  }
`;
//
export const restarigo = gql`
  mutation restarigo($login: String!, $kodo: String, $password: String) {
    restarigo(ensalutu: $login, konfirmaKodo: $kodo, pasvorto: $password) {
      status
      message
    }
  }
`;
//
export const registraKonfirmo = gql`
  mutation registraKonfirmo(
    $login: String!
    $password: String!
    $confirm: String!
  ) {
    registraKonfirmo(
      ensalutu: $login
      pasvorto: $password
      konfirmaKodo: $confirm
    ) {
      status
      message
    }
  }
`;
//
export const novaKonfirmaKodo = gql`
  mutation novaKonfirmaKodo($login: String!, $password: String!) {
    novaKonfirmaKodo(ensalutu: $login, pasvorto: $password) {
      status
      message
    }
  }
`;
//
export const registrado = gql`
  mutation registrado(
    $email: String!
    $paswd: String!
    $first_name: String!
    $last_name: String!
    $lang: String
    $tel: String
    $sex: String
    $captcha: String!
  ) {
    registrado(
      chefaRetposhto: $email
      pasvorto: $paswd
      unuaNomo: $first_name
      familinomo: $last_name
      chefaTelefonanumero: $tel
      lingvo: $lang
      sekso: $sex
      reCaptcha: $captcha
    ) {
      status
      errors {
        field
        message
      }
      message
    }
  }
`;
//
export const instaliAvataron = gql`
  mutation instaliAvataron($id: Int!) {
    instaliAvataron(uzantoId: $id) {
      status
      message
      errors {
        field
        message
      }
      avataro {
        bildoE {
          url
        }
        bildoF {
          url
        }
      }
    }
  }
`;
