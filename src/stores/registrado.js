import { defineStore } from "pinia";

import {
  informilojLingvoj_informilojLandoj,
  informilojRegionoj,
  komunumojQuery,
} from "src/queries/queries";
import { apollo } from "src/boot/apollo";
import {
  novaKonfirmaKodo,
  registrado,
  registraKonfirmo,
  restarigo,
} from "src/queries/mutations";
import { debugLog } from "src/utils";
import { useUiStore } from "./UI";

export const useRegistradoStore = defineStore("registrado", {
  state: () => {
    return {
      infoj: {
        lingvoj: [],
        landoj: [],
        tel_kodoj: [],
        regiono: [],
        organizo: [],
      },
      email: null,
      password: null,
    };
  },
  getters: {
    getInfoj: (state) => state.infoj,
    getEmail: (state) => state.email,
    getPassword: (state) => state.password,
  },

  actions: {
    setEmail(payload) {
      this.email = payload;
    },
    setPassword(payload) {
      this.password = payload;
    },
    setInfoj(payload) {
      this.infoj.lingvoj = payload.data.informilojLingvoj.edges.map((v) => ({
        id: v.node.uuid,
        label: v.node.nomo,
        value: v.node.kodo,
        flag: v.node.flago,
      }));
      this.infoj.landoj = payload.data.informilojLandoj.edges.map((v) => ({
        id: v.node.uuid,
        label: v.node.nomo.enhavo,
        value: v.node.kodo,
        flag: v.node.flago,
      }));
      this.infoj.tel_kodoj = payload.data.informilojLandoj.edges.map((v) => ({
        label: v.node.telefonakodo,
        value: v.node.telefonakodo,
        flag: v.node.flago,
        id: v.node.uuid,
        nomo: v.node.nomo.enhavo,
        kodo: v.node.kodo,
      }));
    },
    setInfojRegiono(payload) {
      this.infoj.regiono = payload.data.informilojRegionoj.edges.map((v) => ({
        label: v.node.nomo.enhavo,
        value: v.node.kodo,
        id: v.node.uuid,
      }));
    },
    setOrganizo(payload) {
      this.infoj.organizo = payload.komunumoj.edges.map((v) => ({
        label: v.node.nomo.enhavo,
        value: v.node.objId,
        avataro: v.node.avataro.bildoE.url,
        id: v.node.id,
        priskribo: v.node.priskribo.enhavo,
        uuid: v.node.uuid,
        statistiko: v.node.statistiko,
      }));
    },
    addUzanto(payload) {
      const ui = useUiStore();
      ui.showIsLoading();
      return apollo.registrado
        .mutate({
          mutation: registrado,
          variables: payload,
          update: () => {
            debugLog("Регистрируем пользователя");
          },
        })
        .then((data) => {
          ui.hideIsLoading();
          const resp = data.data.registrado;
          return Promise.resolve(resp);
        })
        .catch((error) => {
          ui.hideIsLoading();
          console.error(error);
          return Promise.reject(error);
        });
    },
    fetchInfoj() {
      if (process.env.SERVER) return;

      return apollo.default
        .query({
          query: informilojLingvoj_informilojLandoj,
          update: () => {
            debugLog("Поехали");
          },
        })
        .then((data) => {
          this.setInfoj(data);
        })
        .catch((error) => {
          console.log(error);
        });
    },
    fetchInfojRegiono(vars) {
      return apollo.default
        .query({
          query: informilojRegionoj,
          variables: vars,
          update: () => {},
        })
        .then((data) => {
          this.setInfojRegiono(data);
        })
        .catch((error) => {
          console.log(error);
        });
    },

    resendConfirm(vars) {
      const ui = useUiStore();

      ui.showIsLoading();

      return apollo.registrado
        .mutate({
          mutation: novaKonfirmaKodo,
          variables: vars,
        })
        .then((data) => {
          ui.hideIsLoading();
          const resp = data.data.novaKonfirmaKodo;
          return Promise.resolve(resp);
        })
        .catch((error) => {
          ui.hideIsLoading();
          console.error(error);
          return Promise.reject(error);
        });
    },
    confirm(vars) {
      const ui = useUiStore();

      ui.showIsLoading();

      return apollo.registrado
        .mutate({
          mutation: registraKonfirmo,
          variables: vars,
        })
        .then((data) => {
          ui.hideIsLoading();
          const resp = data.data.registraKonfirmo;
          return Promise.resolve(resp);
        })
        .catch((error) => {
          ui.hideIsLoading();
          console.error(error);
          return Promise.reject(error);
        });
    },

    restarigi(vars) {
      const ui = useUiStore();

      ui.showIsLoading();

      return apollo.registrado
        .mutate({
          mutation: restarigo,
          variables: vars,
        })
        .then((data) => {
          ui.hideIsLoading();
          const resp = data.data.restarigo;
          return Promise.resolve(resp);
        })
        .catch((error) => {
          ui.hideIsLoading();
          console.error(error);
          return Promise.reject(error);
        });
    },
    fetchOrganizo() {
      const ui = useUiStore();

      ui.showIsLoading();
      return apollo.default
        .query({
          query: komunumojQuery,
          variables: { first: 100, tipo: "entrepreno" },
        })
        .then(({ data }) => {
          this.setOrganizo(data);
          ui.hideIsLoading();
        })
        .catch(() => {
          ui.hideIsLoading();
        });
    },
  },
});
