import { defineStore } from "pinia";

import { uzantoByObjIdQuery } from "src/queries/queries";
import {
  changeAvatar,
  changePassword,
  changeSettings,
  changeTelephoneNumberOrEmail,
} from "src/queries/mutations";
import { apollo } from "src/boot/apollo";
import { debugLog } from "src/utils";
import { useUiStore } from "./UI";
import { useAuthStore } from "./auth";

export const useUserStore = defineStore("user", {
  state() {
    return {
      item: null,
      types: null,
    };
  },
  getters: {
    getUzanto: (state) => (state.item ? state.item.uzanto : null),
  },

  actions: {
    setUzanto(payload) {
      this.item = payload;
    },

    fetchUzanto({ id }) {
      const ui = useUiStore();

      ui.showIsLoading();

      return apollo.default
        .query({
          query: uzantoByObjIdQuery,
          variables: { id },
          errorPolicy: "all",
        })
        .then(({ data }) => {
          this.item = data;
          ui.hideIsLoading();
        })
        .catch(() => {
          ui.hideIsLoading();
        });
    },
    changeUzanto(payload) {
      const ui = useUiStore();
      const auth = useAuthStore();

      ui.showIsLoading();

      return apollo.default
        .mutate({
          mutation: changeSettings,
          variables: payload,
          update: () => {
            debugLog("Вносим изменения");
          },
        })
        .then((data) => {
          ui.hideIsLoading();
          const resp = data.data.aplikiAgordoj;
          auth.getMe();
          return Promise.resolve(resp);
        })
        .catch((error) => {
          ui.hideIsLoading();
          console.error(error);
        });
    },
    changePassword(payload) {
      const ui = useUiStore();
      const auth = useAuthStore();

      ui.showIsLoading();

      const { uzantoId, pasvorto, novaPasvorto } = payload;
      if (auth.isAdmin) {
        payload = { uzantoId, pasvorto, novaPasvorto };
      } else {
        payload = { pasvorto, novaPasvorto };
      }
      return apollo.default
        .mutate({
          mutation: changePassword,
          variables: payload,
          update: () => {
            debugLog("Вносим изменения");
          },
        })
        .then((data) => {
          ui.hideIsLoading();
          const resp = data.data.shanghiPasvorton;
          if (resp.status) {
            auth.csrftoken = resp.csrfToken;
          }
          return Promise.resolve(resp);
        })
        .catch((error) => {
          ui.hideIsLoading();
          console.error(error);

          return Promise.resolve();
        });
    },

    changeTelephoneOrEmail(payload) {
      const ui = useUiStore();
      const auth = useAuthStore();

      ui.showIsLoading();

      return apollo.default
        .mutate({
          mutation: changeTelephoneNumberOrEmail,
          variables: payload,
          update: () => {
            debugLog("Вносим изменения");
          },
        })
        .then((data) => {
          ui.hideIsLoading();
          const resp = data.data.shanghiPoshtonTelefonon;
          if (Object.prototype.hasOwnProperty.call(payload, "code")) {
            auth.getMe();
          }
          return Promise.resolve(resp);
        })
        .catch((error) => {
          ui.hideIsLoading();
          console.error(error);
        });
    },
    changeAvatar(payload) {
      const ui = useUiStore();

      ui.showIsLoading();

      return apollo.default
        .mutate({
          mutation: changeAvatar,
          variables: payload,
          update: () => {
            debugLog("Вносим изменения");
          },
        })
        .then((data) => {
          ui.hideIsLoading();
          const resp = data.data.instaliAvataron;
          return Promise.resolve(resp);
        })
        .catch((error) => {
          ui.hideIsLoading();
          console.error(error);
        });
    },

    refetchUzanto({ id }) {
      const ui = useUiStore();

      ui.showIsLoading();

      return apollo.default
        .query({
          query: uzantoByObjIdQuery,
          variables: { id },
          errorPolicy: "all",
          fetchPolicy: "network-only",
        })
        .then(({ data }) => {
          this.item = data;
          ui.hideIsLoading();
        })
        .catch(() => {
          ui.hideIsLoading();
        });
    },
  },
});
