import { defineStore } from "pinia";
import { LocalStorage } from "quasar";

export const useUiStore = defineStore("UI", {
  state: () => ({
    isLoading: 0,
    dark: false,
    loading: "lazy", //'eager'/'lazy'
    showLoginForm: false,
    localStoredUi: {},
  }),

  getters: {
    getIsLoading(state) {
      return state.isLoading > 0 ? true : false;
    },
    getImageLoadingMode(state) {
      return state.loading;
    },
  },

  actions: {
    getLocalStoredUi() {
      if (LocalStorage.has("ui_store")) {
        this.localStoredUi = LocalStorage.getItem("ui_store") || {};
      }
    },
    setLocalStoredUi(payload) {
      for (const [key, value] of Object.entries(payload)) {
        this.localStoredUi[key] = value;
      }
      LocalStorage.set("ui_store", this.localStoredUi);
    },
    showIsLoading() {
      this.isLoading++;
    },
    hideIsLoading() {
      this.isLoading > 0 ? this.isLoading-- : (this.isLoading = 0);
    },
  },
});
