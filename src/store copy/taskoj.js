import {
  universoProjektojTaskoKategorioQuery,
  universoProjektojTaskoStatusoQuery,
  universoProjektojTaskoTipoQuery
} from 'src/queries/queries';
import { apollo } from 'src/boot/apollo';

export default {
  namespaced: true,
  state() {
    return {
      list: null,
      types: null,
      categories: null,
      statuses: null
    };
  },
  getters: {
    // getTaskoj: state => state.list ? state.list:null,
    getTipoj: (state) => (state.types ? state.types.tipo : null),
    getKategorioj: (state) =>
      state.categories ? state.categories.kategorio : null,
    getStatusoj: (state) => (state.statuses ? state.statuses.statuso : null)
  },
  mutations: {
    // setTaskoj: (state, payload) => (state.list = payload),
    setTipoj: (state, payload) => (state.types = payload),
    setKategorioj: (state, payload) => (state.categories = payload),
    setStatusoj: (state, payload) => (state.statuses = payload)
  },
  actions: {
    // fetchTaskoj({ state, commit }, { first, komunumoId }) {
    // this.dispatch("UIstore/showLoading");
    //   return graphqlClient
    //     .query({
    //       query: projektojQuery,
    //       variables: { first, komunumoId },
    //       errorPolicy: "all"
    //     })
    //     .then(({ data }) => {
    //       commit("setTaskoj", data);
    //       this.dispatch("UIstore/hideLoading");
    //         })
    //         .catch(err => {
    //           this.dispatch("UIstore/hideLoading");
    //         });
    // },
    fetchKategorioj({ commit }) {
      this.dispatch('UIstore/showLoading');
      return apollo.default
        .query({
          query: universoProjektojTaskoKategorioQuery,
          errorPolicy: 'all',
          fetchPolicy: 'network-only'
        })
        .then(({ data }) => {
          commit('setKategorioj', data);
          this.dispatch('UIstore/hideLoading');
        })
        .catch((err) => {
          this.dispatch('UIstore/hideLoading');
        });
    },
    fetchStatusoj({ commit }) {
      this.dispatch('UIstore/showLoading');
      return apollo.default
        .query({
          query: universoProjektojTaskoStatusoQuery,
          errorPolicy: 'all',
          fetchPolicy: 'network-only'
        })
        .then(({ data }) => {
          commit('setStatusoj', data);
          this.dispatch('UIstore/hideLoading');
        })
        .catch((err) => {
          this.dispatch('UIstore/hideLoading');
        });
    },
    fetchTipoj({ commit }) {
      this.dispatch('UIstore/showLoading');
      return apollo.default
        .query({
          query: universoProjektojTaskoTipoQuery,
          errorPolicy: 'all',
          fetchPolicy: 'network-only'
        })
        .then(({ data }) => {
          commit('setTipoj', data);
          this.dispatch('UIstore/hideLoading');
        })
        .catch((err) => {
          this.dispatch('UIstore/hideLoading');
        });
    }
    // refetchTaskoj({ commit }, { first, komunumoId }) {
    // this.dispatch("UIstore/showLoading");
    //   return graphqlClient
    //     .query({
    //       query: projektojQuery,
    //       variables: { first, komunumoId },
    //       errorPolicy: "all",
    //       fetchPolicy: "network-only"
    //     })
    //     .then(({ data }) => {
    //       commit("setTaskoj", data);
    //     this.dispatch("UIstore/hideLoading");
    //   })
    // .catch(err => {
    //   this.dispatch("UIstore/hideLoading");
    // });
    // }
  }
};
