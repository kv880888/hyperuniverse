import {
  addTasko,
  arkTasko,
  changeTasko,
  forigoTasko
} from 'src/queries/mutations';
import { UniversoProjektojTaskoNode } from 'src/queries/queries';
import { apollo } from 'src/boot/apollo';

export default {
  namespaced: true,
  state() {
    return { tasko: null };
  },
  getters: {
    getTasko: (state) => (state.tasko ? state.tasko : null)
  },
  mutations: {
    setTasko: (state, payload) => {
      state.tasko = payload;
    }
  },
  actions: {
    fetchTasko({ commit }, payload) {
      this.dispatch('UIstore/showLoading');
      return apollo.default
        .query({
          query: UniversoProjektojTaskoNode,
          variables: payload,
          errorPolicy: 'all',
          fetchPolicy: 'network-only'
        })
        .then(({ data }) => {
          commit('setTasko', data.universoProjektojTasko.edges[0].node);
          this.dispatch('UIstore/hideLoading');
        })
        .catch((err) => {
          this.dispatch('UIstore/hideLoading');
        });
    },

    // eslint-disable-next-line no-empty-pattern
    addTasko({}, payload) {
      this.dispatch('UIstore/showLoading');
      return apollo.default
        .mutate({
          mutation: addTasko,
          variables: payload,
          update: () => {
            console.log('Регистрируем задачу');
          }
        })
        .then((data) => {
          this.dispatch('UIstore/hideLoading');
          this.commit(
            'projekto/setAddTasko',
            data.data.redaktuUniversoProjektojTaskoj.universoTaskoj
          );
          if (data?.data?.redaktuUniversoProjektojTaskoj?.status) {
            console.log(
              `Задача ${data.data.redaktuUniversoProjektojTaskoj.universoTaskoj?.uuid} успешно зарегистрирована.\nОтвет сервера: ${data.data.redaktuUniversoProjektojTaskoj.message}`
            );
          } else {
            console.log(
              `Задача не зарегистрирована. \nОшибка: ${
                data?.data?.redaktuUniversoProjektojTaskoj?.message ??
                'Неизвестно'
              }`
            );
          }
        })
        .catch((error) => {
          this.dispatch('UIstore/hideLoading');
          console.error(error);
        });
    },
    deleteTasko({ commit }, { id }) {
      this.dispatch('UIstore/showLoading');
      return apollo.default
        .mutate({
          mutation: forigoTasko,
          variables: { id, forigo: true },
          update: () => {
            console.log('Удаляем задачу');
          }
        })
        .then((data) => {
          this.commit('projekto/setDelTasko', id);
          this.dispatch('UIstore/hideLoading');
          if (data?.data?.redaktuUniversoProjektojTaskoj?.status) {
            console.log(
              `Задача ${id} успешно удалена.\nОтвет сервера: ${data.data.redaktuUniversoProjektojTaskoj.message}`
            );
          } else {
            console.log(
              `Задача ${id} не удалена. \nОшибка: ${
                data?.data?.redaktuUniversoProjektojProjekto?.message ??
                'Неизвестно'
              }`
            );
          }
        })
        .catch((error) => {
          this.dispatch('UIstore/hideLoading');
          console.error(error);
        });
    },
    arkTasko({ commit }, { id }) {
      this.dispatch('UIstore/showLoading');
      return apollo.default
        .mutate({
          mutation: arkTasko,
          variables: { id, forigo: true },
          update: () => {
            console.log('Архивируем задачу');
          }
        })
        .then((data) => {
          this.commit('projekto/setDelTasko', id);
          this.dispatch('UIstore/hideLoading');
          if (data?.data?.redaktuUniversoProjektojTaskoj?.status) {
            console.log(
              `Задача ${id} успешно архивирована.\nОтвет сервера: ${data.data.redaktuUniversoProjektojTaskoj.message}`
            );
          } else {
            console.log(
              `Задача ${id} не архивирована. \nОшибка: ${
                data?.data?.redaktuUniversoProjektojProjekto?.message ??
                'Неизвестно'
              }`
            );
          }
        })
        .catch((error) => {
          this.dispatch('UIstore/hideLoading');
          console.error(error);
        });
    },
    changeTasko({ commit }, payload) {
      this.dispatch('UIstore/showLoading');
      return apollo.default
        .mutate({
          mutation: changeTasko,
          variables: payload,
          update: () => {
            console.log('Вносим изменения в задачу');
          }
        })
        .then((data) => {
          this.dispatch('UIstore/hideLoading');
          if (data?.data?.redaktuUniversoProjektojTaskoj?.status) {
            commit(
              'setTasko',
              data.data.redaktuUniversoProjektojTaskoj.universoTaskoj
            );

            console.log(
              `Задача ${payload.uuid} успешно изменена.\nОтвет сервера: ${data.data.redaktuUniversoProjektojTaskoj.message}`
            );
            return Promise.resolve(
              data.data.redaktuUniversoProjektojTaskoj.universoTaskoj
            );
          } else {
            console.log(
              `Задача ${payload.uuid} не изменена. \nОшибка: ${
                data?.data?.redaktuUniversoProjektojProjekto?.message ??
                'Неизвестно'
              }`
            );
          }
        })
        .catch((error) => {
          this.dispatch('UIstore/hideLoading');
          console.error(error);
        });
    }
  }
};
