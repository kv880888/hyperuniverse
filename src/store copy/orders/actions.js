import { addRequest } from 'src/queries/mutations';
import { apollo } from 'src/boot/apollo';
import { logErrorMessages } from '@vue/apollo-util';

/*
export function someAction (context) {
}
*/
export function sendOrder({ commit }, payload) {
  this.dispatch('UIstore/showLoading');
  return apollo.default
    .mutate({
      mutation: addRequest,
      variables: payload,
      update: () => {
        // eslint-disable-next-line no-console
        console.log('Отправляем заказ');
      }
    })
    .then(({ data }) => {
      this.dispatch('UIstore/hideLoading');
      return Promise.resolve(data);
    })
    .catch((error) => {
      this.dispatch('UIstore/hideLoading');
      logErrorMessages(error);
    });
}
