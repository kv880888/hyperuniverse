// default src/store/index.js content:
import { createStore } from 'vuex';
import UIstore from 'src/store/UIstore';
import enketa from 'src/store/enketa';
import siriusoauth from './siriusoauth';
import projektoj from './projektoj';
import tutaProjektoj from './tutaProjektoj';
import uzanto from './uzanto';
import projekto from './projekto';
import registrado from './registrado';
import tasko from 'src/store/tasko';
import taskoj from 'src/store/taskoj';
import ligoj from 'src/store/ligoj';
import dokumentoj from 'src/store/dokumentoj';
import dokumento from 'src/store/dokumento';
import orders from 'src/store/orders';
import websocket from 'src/store/websocket';
let store = null;

export default function (/* { ssrContext } */) {
  const Store = createStore({
    modules: {
      UIstore,
      enketa,
      siriusoauth,
      projektoj,
      tutaProjektoj,
      uzanto,
      projekto,
      registrado,
      ligoj,
      tasko,
      taskoj,
      dokumento,
      dokumentoj,
      orders,
      websocket
    },

    // enable strict mode (adds overhead!)
    // for dev mode and --debug builds only
    strict: process.env.DEBUGGING
  });
  store = Store;

  return Store;
}
export { store };

// import Vue from 'vue';
// import Vuex from 'vuex';
// import UIstore from 'src/store/UIstore';
//
// // import example from './module-example'
//
// Vue.use(Vuex);
//
// /*
//  * If not building with SSR mode, you can
//  * directly export the Store instantiation;
//  *
//  * The function below can be async too; either use
//  * async/await or return a Promise which resolves
//  * with the Store instance.
//  */
//
// export default function(/* { ssrContext } */) {
//   const Store = new Vuex.Store({
//     modules: {
//       UIstore,
//     },
//
//     // enable strict mode (adds overhead!)
//     // for dev mode only
//     strict: process.env.DEV,
//   });
//
//   return Store;
// }
