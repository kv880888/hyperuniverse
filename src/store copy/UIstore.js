export default {
  namespaced: true,
  state() {
    return {
      isLoading: 0,
      dark: false,
      stateUI: null,
      showLoginForm: false,
      videoOn: false,
      loading: 'lazy' //'eager'/'lazy'
    }
  },
  getters: {
    getIsLoading: (state) => (state.isLoading > 0 ? true : false),
    getDark: (state) => state.dark,
    getStateUI: (state) => state.stateUI,
    getShowLoginForm: (state) => state.showLoginForm,
    getVideoState(state) {
      return state.videoOn
    },
    getLoadingMode(state) {
      return state.loading
    }
  },
  mutations: {
    showLoginForm(state, payload) {
      state.showLoginForm = payload
    },
    showIsLoading(state) {
      state.isLoading++
    },
    hideIsLoading(state) {
      // eslint-disable-next-line no-unused-expressions
      state.isLoading !== 0 ? state.isLoading-- : (state.isLoading = 0)
    },
    setDark(state, payload) {
      state.dark = payload
      localStorage.setItem('dark', payload)
    },
    setStateUI(state, payload) {
      state.stateUI = Object.assign({}, state.stateUI, payload)
      localStorage.setItem('stateUI', JSON.stringify(state.stateUI))
    },
    resetIsLoading(state) {
      state.isLoading = 0
    },
    setVideoState(state, payload) {
      state.videoOn = payload
      localStorage.setItem('videoOn', payload)
    }
  },
  actions: {
    showLoading(store) {
      store.commit('showIsLoading')
      setTimeout(() => {
        store.commit('resetIsLoading')
      }, 15000)
    },
    hideLoading(store) {
      setTimeout(() => {
        store.commit('hideIsLoading')
      }, 500)
    }
  }
}
