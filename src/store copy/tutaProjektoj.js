import { tutaProjektojQuery } from 'src/queries/queries';
import { addProjekto, forigoProjekto } from 'src/queries/mutations';
import { apollo } from 'src/boot/apollo';

export default {
  namespaced: true,
  state() {
    return {
      edges: [],
      pageInfo: null,
      needRefresh: false
    };
  },
  getters: {
    getTutaProjektoj: (state) => {
      return state.edges.length > 0
        ? {
            pageInfo: state.pageInfo,
            edges: state.edges.map((item) => item[1])
          }
        : null;
    }
  },
  mutations: {
    setTutaProjektoj(state, payload) {
      let mapList;
      if (state.needRefresh === false) {
        mapList = new Map(state.edges);
      } else {
        mapList = new Map();
      }
      payload.tutaProjektoj.edges.forEach((item) => {
        mapList.set(item.node.uuid, item);
      });
      state.pageInfo = payload.tutaProjektoj.pageInfo;
      state.edges = Array.from(mapList);
      state.needRefresh = false;
    },
    setNeedRefresh(state) {
      state.needRefresh = true;
      state.pageInfo = null;
    },

    setAddProjekto(state, payload) {
      state.edges.unshift([payload.uuid, { node: payload }]);
      state.needRefresh = true;
      state.pageInfo = null;
      this.commit('projektoj/setNeedRefresh');
    },

    setDelProjekto(state, payload) {
      const mapList = new Map(state.edges);
      if (mapList.has(payload)) {
        mapList.delete(payload);
      }
      state.edges = Array.from(mapList);
      state.needRefresh = true;
      state.pageInfo = null;
      // this.commit('projektoj/setDelProjekto',payload)
      this.commit('projektoj/setNeedRefresh');
    }
  },
  actions: {
    addProjektoj({ commit }, payload) {
      this.dispatch('UIstore/showLoading');
      return apollo.default
        .mutate({
          mutation: addProjekto,
          variables: payload,
          update: () => {
            console.log('Регистрируем проект');
          }
        })
        .then((data) => {
          this.dispatch('UIstore/hideLoading');
          if (data?.data?.redaktuUniversoProjektojProjekto?.status) {
            console.log(
              `Проект ${data.data.redaktuUniversoProjektojProjekto.universoProjekto?.uuid} успешно зарегистрирован.\nОтвет сервера: ${data.data.redaktuUniversoProjektojProjekto.message}`
            );
            commit(
              'setAddProjekto',
              data.data.redaktuUniversoProjektojProjekto.universoProjekto
            );
          } else {
            console.log(
              `Проект не зарегистрирован. \nОшибка: ${
                data?.data?.redaktuUniversoProjektojProjekto?.message ??
                'Неизвестно'
              }`
            );
          }
        })
        .catch((error) => {
          console.error(error);
          this.dispatch('UIstore/hideLoading');
        });
    },

    fetchTutaProjektoj({ state, commit }, { first, after }) {
      this.dispatch('UIstore/showLoading');
      return apollo.default
        .query({
          query: tutaProjektojQuery,
          variables: { first, after },
          errorPolicy: 'all',
          fetchPolicy: 'network-only'
        })
        .then(({ data }) => {
          commit('setTutaProjektoj', data);
          this.dispatch('UIstore/hideLoading');
        })
        .catch((err) => {
          this.dispatch('UIstore/hideLoading');
        });
    },
    delProjekto({ commit }, { id }) {
      console.log(id);
      this.dispatch('UIstore/showLoading');
      return apollo.default
        .mutate({
          mutation: forigoProjekto,
          variables: { id, forigo: true },
          update: () => {
            console.log('Удаляем проект');
          }
        })
        .then((data) => {
          this.dispatch('UIstore/hideLoading');
          if (data?.data?.redaktuUniversoProjektojProjekto?.status) {
            console.log(
              `Проект ${id} успешно удален.\nОтвет сервера: ${data.data.redaktuUniversoProjektojProjekto.message}`
            );
            commit('setDelProjekto', id);
          } else {
            console.log(
              `Проект ${id} не удален. \nОшибка: ${
                data?.data?.redaktuUniversoProjektojProjekto?.message ??
                'Неизвестно'
              }`
            );
          }
        })
        .catch((error) => {
          this.dispatch('UIstore/hideLoading');
          console.error(error);
        });
    },
    refetchTutaProjektoj({ state, commit }, { first, after }) {
      this.dispatch('UIstore/showLoading');
      return apollo.default
        .query({
          query: tutaProjektojQuery,
          variables: { first, after },
          errorPolicy: 'all',
          fetchPolicy: 'network-only'
        })
        .then(({ data }) => {
          commit('setTutaProjektoj', data);
          this.dispatch('UIstore/hideLoading');
        })
        .catch((err) => {
          this.dispatch('UIstore/hideLoading');
        });
    }
  }
};
