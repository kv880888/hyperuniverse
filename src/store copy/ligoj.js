import {
  ligojTipoj,
  projektojQuerySimple,
  UniversoProjektojProjektoLigilo
} from 'src/queries/queries';
import { changeLigilo } from 'src/queries/mutations';
import { apollo } from 'src/boot/apollo';

export default {
  namespaced: true,
  state() {
    return {
      list: [],
      tipoj: null,
      projektoj: null,
      objId: null
    };
  },
  getters: {
    getLigoj: (state) => {
      return state;
    }
  },
  mutations: {
    setLigoj: (state, pload) => {
      let mapList;
      const { objId, payload } = pload;
      if (objId === state.objId) {
        mapList = new Map(state.list);
      } else {
        mapList = new Map();
      }

      function addItemToMap(cur) {
        payload[cur].edges.forEach((item) => {
          mapList.set(item.node.uuid, {
            node: {
              item: item.node[cur],
              uuid: item.node.uuid,
              tipo: item.node.tipo,
              ligilo: cur,
              objId: item.node.objId
            }
          });
        });
      }

      addItemToMap('ligilo');
      addItemToMap('posedanto');
      state.list = Array.from(mapList);
      state.objId = objId;
    },
    setTipoj: (state, payload) => {
      state.tipoj = payload;
    },
    setProjektoj: (state, payload) => {
      state.projektoj = payload;
    },
    delLigoj: (state, uuid) => {
      const mapList = new Map(state.list);
      if (mapList.has(uuid)) {
        mapList.delete(uuid);
      }
      state.list = Array.from(mapList);
    },
    changeLigilo: (state, payload) => {
      const uuid = payload.universoProjektoLigilo.uuid;
      const tipo = payload.universoProjektoLigilo.tipo;

      const mapList = new Map(state.list);

      if (mapList.has(uuid)) {
        const val = mapList.get(uuid);
        val.node.tipo = tipo;
        mapList.set(uuid, val);
      }
      state.list = Array.from(mapList);
    },
    addLigilo: (state, payload) => {
      const val = {
        node: {
          item: payload.universoProjektoLigilo.ligilo,
          tipo: payload.universoProjektoLigilo.tipo,
          uuid: payload.universoProjektoLigilo.uuid,
          ligilo: 'ligilo',
          objId: payload.universoProjektoLigilo.objId
        }
      };
      state.list.unshift([payload.universoProjektoLigilo.uuid, val]);
    }
  },
  actions: {
    fetchLigoj({ commit }, payload) {
      // eslint-disable-next-line camelcase
      const { posedanto_Uuid } = payload;
      this.dispatch('UIstore/showLoading');
      return apollo.default
        .query({
          query: UniversoProjektojProjektoLigilo,
          variables: payload,
          errorPolicy: 'all',
          fetchPolicy: 'network-only'
        })
        .then(({ data }) => {
          commit('setLigoj', { objId: posedanto_Uuid, payload: data });
          this.dispatch('UIstore/hideLoading');
        })
        .catch((err) => {
          this.dispatch('UIstore/hideLoading');
          console.error(err);
        });
    },
    changeLigoj({ commit }, payload) {
      this.dispatch('UIstore/showLoading');
      return apollo.default
        .mutate({
          mutation: changeLigilo,
          variables: payload,
          errorPolicy: 'all',
          update: () => {
            console.log('Вносим изменение');
          }
        })
        .then(({ data }) => {
          this.dispatch('UIstore/hideLoading');

          if (data?.redaktuUniversoProjektojProjektoLigilo?.status) {
            if (
              Object.prototype.hasOwnProperty.call(payload, 'forigo') &&
              payload.forigo === true
            ) {
              console.log(
                `Связь успешно удалена.\nОтвет сервера: ${data.redaktuUniversoProjektojProjektoLigilo.message}`
              );
              commit('delLigoj', payload.uuid);
            } else if (Object.prototype.hasOwnProperty.call(payload, 'uuid')) {
              console.log(
                `Связь успешно изменена.\nОтвет сервера: ${data.redaktuUniversoProjektojProjektoLigilo.message}`
              );
              commit(
                'changeLigilo',
                data.redaktuUniversoProjektojProjektoLigilo
              );
            } else {
              console.log(
                `Связь успешно создана.\nОтвет сервера: ${data.redaktuUniversoProjektojProjektoLigilo.message}`
              );
              commit('addLigilo', data.redaktuUniversoProjektojProjektoLigilo);
            }
          } else {
            console.log(
              `Связь не изменена. \nОшибка: ${
                data?.redaktuUniversoProjektojProjektoLigilo?.message ??
                'Неизвестно'
              }`
            );
          }
          return Promise.resolve(data.redaktuUniversoProjektojProjektoLigilo);
        })
        .catch((err) => {
          this.dispatch('UIstore/hideLoading');
          console.error(err);
        });
    },

    fetchTipoj({ commit }) {
      this.dispatch('UIstore/showLoading');
      return apollo.default
        .query({
          query: ligojTipoj,
          errorPolicy: 'all',
          fetchPolicy: 'network-only'
        })
        .then(({ data }) => {
          commit('setTipoj', data.universoProjektojProjektoLigiloTipo?.edges);
          this.dispatch('UIstore/hideLoading');
        })
        .catch((err) => {
          this.dispatch('UIstore/hideLoading');
          console.error(err);
        });
    },
    fetchProjektojSimple({ commit }) {
      this.dispatch('UIstore/showLoading');
      return apollo.default
        .query({
          query: projektojQuerySimple,
          errorPolicy: 'all',
          fetchPolicy: 'network-only'
        })
        .then(({ data }) => {
          commit('setProjektoj', data.projektojSimple?.edges);
          this.dispatch('UIstore/hideLoading');
        })
        .catch((err) => {
          this.dispatch('UIstore/hideLoading');
          console.error(err);
        });
    }
  }
};
