import { projektoByUuidQuery } from 'src/queries/queries';
import {
  arkProjekto,
  changeProjekto,
  forigoProjekto
} from 'src/queries/mutations';
import { apollo } from 'src/boot/apollo';

export default {
  namespaced: true,
  state() {
    return {
      item: null
    };
  },
  getters: {
    getProjekto: (state) => (state.item ? state.item.projekto : null)
  },
  mutations: {
    setProjekto: (state, payload) => {
      state.item = payload;
    },
    setChangeProjekto: (state, payload) => {
      state.item.projekto.edges[0].node = payload;
    },
    setAddTasko: (state, payload) => {
      const node = { node: payload };
      state.item.projekto.edges[0].node.tasko.edges.unshift(node);
    },
    setDelTasko: (state, payload) => {
      let arr = state?.item?.projekto?.edges[0]?.node?.tasko?.edges;
      arr = arr.filter((item) => item.node.uuid !== payload);
      state.item.projekto.edges[0].node.tasko.edges = arr;

      // for (let n = 0; n < arr.length; n++) {
      //   if (arr[n]?.node.uuid == payload) {
      //     let removedObject = arr.splice(n, 1);
      //     removedObject = null;
      //     break;
      //   }
      // }
    }
  },
  actions: {
    fetchProjekto({ commit }, { objId }) {
      this.dispatch('UIstore/showLoading');
      return apollo.default
        .query({
          query: projektoByUuidQuery,
          variables: { objId },
          errorPolicy: 'all',
          fetchPolicy: 'network-only'
        })
        .then(({ data }) => {
          commit('setProjekto', data);
          this.dispatch('UIstore/hideLoading');
          return Promise.resolve(data);
        })
        .catch((err) => {
          this.dispatch('UIstore/hideLoading');
        });
    },

    delProjekto({ commit }, { id }) {
      this.dispatch('UIstore/showLoading');
      return apollo.default
        .mutate({
          mutation: forigoProjekto,
          variables: { id, forigo: true },
          update: () => {
            console.log('Удаляем проект');
          }
        })
        .then((data) => {
          this.dispatch('UIstore/hideLoading');
          if (data?.data?.redaktuUniversoProjektojProjekto?.status) {
            console.log(
              `Проект ${id} успешно удален.\nОтвет сервера: ${data.data.redaktuUniversoProjektojProjekto.message}`
            );
            this.commit('projektoj/setDelProjekto', id);
            this.commit('tutaProjektoj/setDelProjekto', id);
          } else {
            console.log(
              `Проект ${id} не удален. \nОшибка: ${
                data?.data?.redaktuUniversoProjektojProjekto?.message ??
                'Неизвестно'
              }`
            );
          }
        })
        .catch((error) => {
          this.dispatch('UIstore/hideLoading');
          console.error(error);
        });
    },
    arkProjekto({ commit }, { id }) {
      this.dispatch('UIstore/showLoading');
      return apollo.default
        .mutate({
          mutation: arkProjekto,
          variables: { id, arkivo: true },
          update: () => {
            console.log('Архивируем проект');
          }
        })
        .then((data) => {
          this.dispatch('UIstore/hideLoading');
          if (data?.data?.redaktuUniversoProjektojProjekto?.status) {
            console.log(
              `Проект ${id} успешно отправлен в архив.\nОтвет сервера: ${data.data.redaktuUniversoProjektojProjekto.message}`
            );
            this.commit('projektoj/setDelProjekto', id);
            this.commit('tutaProjektoj/setDelProjekto', id);
          } else {
            console.log(
              `Проект ${id} не архивирован. \nОшибка: ${
                data?.data?.redaktuUniversoProjektojProjekto?.message ??
                'Неизвестно'
              }`
            );
          }
        })
        .catch((error) => {
          this.dispatch('UIstore/hideLoading');
          console.error(error);
        });
    },
    changeProjekto({ commit }, { nomo, priskribo, uuid }) {
      this.dispatch('UIstore/showLoading');
      return apollo.default
        .mutate({
          mutation: changeProjekto,
          variables: {
            nomo,
            uuid,
            priskribo
          },
          update: () => {
            console.log('Вносим изменения в проект');
          }
        })
        .then((data) => {
          this.dispatch('UIstore/hideLoading');
          if (data?.data?.redaktuUniversoProjektojProjekto?.status) {
            console.log(
              `Проект ${data.data.redaktuUniversoProjektojProjekto.universoProjekto?.uuid} успешно изменен.\nОтвет сервера: ${data.data.redaktuUniversoProjektojProjekto.message}`
            );
            commit(
              'setChangeProjekto',
              data.data.redaktuUniversoProjektojProjekto.universoProjekto
            );
          } else {
            console.log(
              `Проект не изменён. \nОшибка: ${
                data?.data?.redaktuUniversoProjektojProjekto?.message ??
                'Неизвестно'
              }`
            );
          }
        })
        .catch((error) => {
          console.error(error);
          this.dispatch('UIstore/hideLoading');
        });
    }
  }
};
