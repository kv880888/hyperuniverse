import { uzantoByObjIdQuery } from 'src/queries/queries';
import {
  changeAvatar,
  changePassword,
  changeSettings,
  changeTelephoneNumberOrEmail
} from 'src/queries/mutations';
import { apollo } from 'src/boot/apollo';

export default {
  namespaced: true,
  state() {
    return {
      item: null,
      types: null
    };
  },
  getters: {
    getUzanto: (state) => (state.item ? state.item.uzanto : null)
  },
  mutations: {
    setUzanto: (state, payload) => (state.item = payload)
  },
  actions: {
    fetchUzanto({ commit }, { id }) {
      this.dispatch('UIstore/showLoading');
      return apollo.default
        .query({
          query: uzantoByObjIdQuery,
          variables: { id },
          errorPolicy: 'all'
        })
        .then(({ data }) => {
          commit('setUzanto', data);
          this.dispatch('UIstore/hideLoading');
        })
        .catch(() => {
          this.dispatch('UIstore/hideLoading');
        });
    },
    changeUzanto(state, payload) {
      this.dispatch('UIstore/showLoading');
      return apollo.default
        .mutate({
          mutation: changeSettings,
          variables: payload,
          update: () => {
            // eslint-disable-next-line no-console
            console.log('Вносим изменения');
          }
        })
        .then((data) => {
          this.dispatch('UIstore/hideLoading');
          const resp = data.data.aplikiAgordoj;
          this.dispatch('siriusoauth/fetchUser');

          return Promise.resolve(resp);
        })
        .catch((error) => {
          this.dispatch('UIstore/hideLoading');
          // eslint-disable-next-line no-console
          console.error(error);
        });
    },
    changePassword(state, payload) {
      this.dispatch('UIstore/showLoading');
      const { uzantoId, pasvorto, novaPasvorto } = payload;
      if (this.getters['siriusoauth/isAdmin']) {
        payload = { uzantoId, pasvorto, novaPasvorto };
      } else {
        payload = { pasvorto, novaPasvorto };
      }
      // eslint-disable-next-line no-console
      return apollo.default
        .mutate({
          mutation: changePassword,
          variables: payload,
          update: () => {
            // eslint-disable-next-line no-console
            console.log('Вносим изменения');
          }
        })
        .then((data) => {
          this.dispatch('UIstore/hideLoading');
          const resp = data.data.shanghiPasvorton;
          // this.dispatch('siriusoauth/fetchUser');
          if (resp.status) {
            this.commit('siriusoauth/setCsrfToken', resp.csrfToken);
          }
          return Promise.resolve(resp);
        })
        .catch((error) => {
          this.dispatch('UIstore/hideLoading');
          // eslint-disable-next-line no-console
          console.error(error);

          return Promise.resolve();
        });
    },

    changeTelephoneOrEmail(state, payload) {
      this.dispatch('UIstore/showLoading');
      return apollo.default
        .mutate({
          mutation: changeTelephoneNumberOrEmail,
          variables: payload,
          update: () => {
            // eslint-disable-next-line no-console
            console.log('Вносим изменения');
          }
        })
        .then((data) => {
          this.dispatch('UIstore/hideLoading');
          const resp = data.data.shanghiPoshtonTelefonon;
          if (Object.prototype.hasOwnProperty.call(payload, 'code')) {
            this.dispatch('siriusoauth/fetchUser');
          }
          return Promise.resolve(resp);
        })
        .catch((error) => {
          this.dispatch('UIstore/hideLoading');
          // eslint-disable-next-line no-console
          console.error(error);
        });
    },
    changeAvatar(state, payload) {
      this.dispatch('UIstore/showLoading');
      // for (const k in payload) {
      //   console.log(k, payload[k]);
      // }
      return apollo.default
        .mutate({
          mutation: changeAvatar,
          variables: payload,
          update: () => {
            // eslint-disable-next-line no-console
            console.log('Вносим изменения');
          }
        })
        .then((data) => {
          this.dispatch('UIstore/hideLoading');
          const resp = data.data.instaliAvataron;
          return Promise.resolve(resp);
        })
        .catch((error) => {
          this.dispatch('UIstore/hideLoading');
          // eslint-disable-next-line no-console
          console.error(error);
        });
    },

    refetchUzanto({ commit }, { id }) {
      this.dispatch('UIstore/showLoading');
      return apollo.default
        .query({
          query: uzantoByObjIdQuery,
          variables: { id },
          errorPolicy: 'all',
          fetchPolicy: 'network-only'
        })
        .then(({ data }) => {
          commit('setUzanto', data);
          this.dispatch('UIstore/hideLoading');
        })
        .catch(() => {
          this.dispatch('UIstore/hideLoading');
        });
    }
  }
};
