import { sendMail } from 'src/queries/mutations';
import { apollo } from 'src/boot/apollo';

export function sendQueryToMail({ commit }, payload) {
  // this.dispatch('UIstore/showLoading');
  return apollo.default
    .mutate({
      mutation: sendMail,
      variables: payload,
      update: () => {
        console.log('Отправляем запрос');
      }
    })
    .then((data) => {
      // this.dispatch('UIstore/hideLoading');
      if (data?.data?.enketo?.status === true) {
        console.log(`Запрос отправлен`);
        // commit('setAddProjekto', data.data.redaktuUniversoProjektojProjekto.universoProjekto);
      } else {
        console.log(`Что-то пошло не так. Запрос не отправлен`);
      }
    })
    .catch((error) => {
      console.error(error);
      // this.dispatch('UIstore/hideLoading');
    });
}
