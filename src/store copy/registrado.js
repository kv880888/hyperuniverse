import gql from 'graphql-tag';
import { komunumojQuery } from 'src/queries/queries';
import { apollo } from 'src/boot/apollo';

export default {
  namespaced: true,
  state() {
    return {
      infoj: {
        lingvoj: [],
        landoj: [],
        tel_kodoj: [],
        regiono: [],
        organizo: []
      },
      email: null,
      password: null
    };
  },
  getters: {
    getInfoj: (state) => state.infoj,
    getEmail: (state) => state.email,
    getPassword: (state) => state.password
  },
  mutations: {
    setEmail: (state, payload) => {
      state.email = payload;
    },
    setPassword: (state, payload) => {
      state.password = payload;
    },
    setInfoj: (state, payload) => {
      state.infoj.lingvoj = payload.data.informilojLingvoj.edges.map((v) => ({
        id: v.node.uuid,
        label: v.node.nomo,
        value: v.node.kodo,
        flag: v.node.flago
      }));
      state.infoj.landoj = payload.data.informilojLandoj.edges.map((v) => ({
        id: v.node.uuid,
        label: v.node.nomo.enhavo,
        value: v.node.kodo,
        flag: v.node.flago
      }));
      state.infoj.tel_kodoj = payload.data.informilojLandoj.edges.map((v) => ({
        label: v.node.telefonakodo,
        value: v.node.telefonakodo,
        flag: v.node.flago,
        id: v.node.uuid,
        nomo: v.node.nomo.enhavo,
        kodo: v.node.kodo
      }));
    },
    setInfojRegiono: (state, payload) => {
      state.infoj.regiono = payload.data.informilojRegionoj.edges.map((v) => ({
        label: v.node.nomo.enhavo,
        value: v.node.kodo,
        id: v.node.uuid
      }));
    },
    setOrganizo: (state, payload) => {
      state.infoj.organizo = payload.komunumoj.edges.map((v) => ({
        label: v.node.nomo.enhavo,
        value: v.node.objId,
        avataro: v.node.avataro.bildoE.url,
        id: v.node.id,
        priskribo: v.node.priskribo.enhavo,
        uuid: v.node.uuid,
        statistiko: v.node.statistiko
      }));
    }
  },
  actions: {
    // eslint-disable-next-line no-empty-pattern
    addUzanto({}, payload) {
      this.dispatch('UIstore/showLoading');
      const mutation = gql`
        mutation (
          $email: String!
          $paswd: String!
          $first_name: String!
          $last_name: String!
          $lang: String
          $tel: String
          $sex: String
          $captcha: String!
        ) {
          registrado(
            chefaRetposhto: $email
            pasvorto: $paswd
            unuaNomo: $first_name
            familinomo: $last_name
            chefaTelefonanumero: $tel
            lingvo: $lang
            sekso: $sex
            reCaptcha: $captcha
          ) {
            status
            errors {
              field
              message
            }
            message
          }
        }
      `;
      return apollo.registrado
        .mutate({
          mutation: mutation,
          variables: payload,
          update: () => {
            console.log('Регистрируем пользователя');
          }
        })
        .then((data) => {
          this.dispatch('UIstore/hideLoading');
          const resp = data.data.registrado;
          // console.log(resp);
          return Promise.resolve(resp);
        })
        .catch((error) => {
          this.dispatch('UIstore/hideLoading');
          console.error(error);
          return Promise.reject(error);
        });
    },
    fetchInfoj({ commit }) {
      if (process.env.SERVER) return;
      const query = gql`
        query {
          informilojLingvoj {
            edges {
              node {
                uuid
                kodo
                nomo
                flago
              }
            }
          }

          informilojLandoj {
            edges {
              node {
                uuid
                flago
                kodo
                nomo {
                  enhavo
                }
                telefonakodo
              }
            }
          }
        }
      `;
      return apollo.default
        .query({
          query: query,
          update: () => {
            console.log('Поехали');
          }
        })
        .then((data) => {
          commit('setInfoj', data);
        })
        .catch((error) => {
          console.log(error);
        });
    },
    fetchInfojRegiono({ commit }, vars) {
      const query = gql`
        query informilojRegionoj($lando_Kodo: String) {
          informilojRegionoj(lando_Kodo: $lando_Kodo) {
            edges {
              node {
                uuid
                kodo
                nomo {
                  enhavo
                }
              }
            }
          }
        }
      `;
      return apollo.default
        .query({
          query: query,
          variables: vars,
          update: () => {}
        })
        .then((data) => {
          commit('setInfojRegiono', data);
        })
        .catch((error) => {
          console.log(error);
        });
    },

    // eslint-disable-next-line no-empty-pattern
    resendConfirm({}, vars) {
      this.dispatch('UIstore/showLoading');
      const mutation = gql`
        mutation ($login: String!, $password: String!) {
          novaKonfirmaKodo(ensalutu: $login, pasvorto: $password) {
            status
            message
          }
        }
      `;
      return apollo.registrado
        .mutate({
          mutation: mutation,
          variables: vars
        })
        .then((data) => {
          this.dispatch('UIstore/hideLoading');
          const resp = data.data.novaKonfirmaKodo;
          return Promise.resolve(resp);
        })
        .catch((error) => {
          this.dispatch('UIstore/hideLoading');
          console.error(error);
          return Promise.reject(error);
        });
    },
    // eslint-disable-next-line no-empty-pattern
    confirm({}, vars) {
      this.dispatch('UIstore/showLoading');
      const mutation = gql`
        mutation ($login: String!, $password: String!, $confirm: String!) {
          registraKonfirmo(
            ensalutu: $login
            pasvorto: $password
            konfirmaKodo: $confirm
          ) {
            status
            message
          }
        }
      `;
      return apollo.registrado
        .mutate({
          mutation: mutation,
          variables: vars
        })
        .then((data) => {
          this.dispatch('UIstore/hideLoading');
          const resp = data.data.registraKonfirmo;
          return Promise.resolve(resp);
        })
        .catch((error) => {
          this.dispatch('UIstore/hideLoading');
          console.error(error);
          return Promise.reject(error);
        });
    },

    // eslint-disable-next-line no-empty-pattern
    restarigi({}, vars) {
      this.dispatch('UIstore/showLoading');
      const mutation = gql`
        mutation ($login: String!, $kodo: String, $password: String) {
          restarigo(
            ensalutu: $login
            konfirmaKodo: $kodo
            pasvorto: $password
          ) {
            status
            message
          }
        }
      `;
      return apollo.registrado
        .mutate({
          mutation: mutation,
          variables: vars
        })
        .then((data) => {
          this.dispatch('UIstore/hideLoading');
          const resp = data.data.restarigo;
          // console.log(resp);
          return Promise.resolve(resp);
        })
        .catch((error) => {
          this.dispatch('UIstore/hideLoading');
          console.error(error);
          return Promise.reject(error);
        });
    },
    fetchOrganizo({ commit }) {
      this.dispatch('UIstore/showLoading');
      return apollo.default
        .query({
          query: komunumojQuery,
          variables: { first: 100, tipo: 'entrepreno' }
        })
        .then(({ data }) => {
          commit('setOrganizo', data);
          this.dispatch('UIstore/hideLoading');
        })
        .catch(() => {
          this.dispatch('UIstore/hideLoading');
        });
    }
  }
};
