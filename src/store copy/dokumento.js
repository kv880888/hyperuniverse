import { Dokumento } from 'src/queries/queries';
import { redaktuDokumento } from 'src/queries/mutations';
import { apollo } from 'src/boot/apollo';

export default {
  namespaced: true,
  state() {
    return {
      item: null
    };
  },
  getters: {
    getDokumento: (state) => (state.item ? state.item.universoDokumento : null)
  },
  mutations: {
    setDokumento: (state, payload) => {
      state.item = payload;
    },
    setChangeDokumento: (state, payload) => {
      state.item.universoDokumento.edges[0].node = payload;
    }
  },
  actions: {
    fetchDokumento({ commit }, payload) {
      this.dispatch('UIstore/showLoading');
      return apollo.default
        .query({
          query: Dokumento,
          variables: payload,
          errorPolicy: 'all',
          fetchPolicy: 'network-only'
        })
        .then(({ data }) => {
          commit('setDokumento', data);
          this.dispatch('UIstore/hideLoading');
          return Promise.resolve(data);
        })
        .catch((err) => {
          this.dispatch('UIstore/hideLoading');
        });
    },

    delDokumento({ commit }, payload) {
      const { uuid } = payload;
      this.dispatch('UIstore/showLoading');
      return apollo.default
        .mutate({
          mutation: redaktuDokumento,
          variables: payload,
          update: () => {
            console.log('Удаляем проект');
          }
        })
        .then((data) => {
          this.dispatch('UIstore/hideLoading');
          if (data?.data?.redaktuUniversoDokumento?.status) {
            console.log(
              `Документ ${uuid} успешно удален.\nОтвет сервера: ${data.data.redaktuUniversoDokumento.message}`
            );
            this.commit('dokumentoj/setDelDokumento', uuid);
          } else {
            console.log(
              `Документ ${uuid} не удален. \nОшибка: ${
                data?.data?.redaktuUniversoDokumento?.message ?? 'Неизвестно'
              }`
            );
          }
        })
        .catch((error) => {
          this.dispatch('UIstore/hideLoading');
          console.error(error);
        });
    },
    arkDokumento({ commit }, payload) {
      const { uuid } = payload;

      this.dispatch('UIstore/showLoading');
      return apollo.default
        .mutate({
          mutation: redaktuDokumento,
          variables: payload,
          update: () => {
            console.log('Архивируем проект');
          }
        })
        .then((data) => {
          this.dispatch('UIstore/hideLoading');
          if (data?.data?.redaktuUniversoDokumento?.status) {
            console.log(
              `Документ ${uuid} успешно отправлен в архив.\nОтвет сервера: ${data.data.redaktuUniversoDokumento.message}`
            );
            this.commit('dokumentoj/setDelDokumento', uuid);
          } else {
            console.log(
              `Документ ${uuid} не архивирован. \nОшибка: ${
                data?.data?.redaktuUniversoDokumento?.message ?? 'Неизвестно'
              }`
            );
          }
        })
        .catch((error) => {
          this.dispatch('UIstore/hideLoading');
          console.error(error);
        });
    },
    changeDokumento({ commit }, payload) {
      this.dispatch('UIstore/showLoading');
      return apollo.default
        .mutate({
          mutation: redaktuDokumento,
          variables: payload,
          update: () => {
            console.log('Вносим изменения в документ');
          }
        })
        .then((data) => {
          this.dispatch('UIstore/hideLoading');
          if (data?.data?.redaktuUniversoDokumento?.status) {
            console.log(
              `Документ ${data.data.redaktuUniversoDokumento.universoDokumentoj?.uuid} успешно изменен.\nОтвет сервера: ${data.data.redaktuUniversoDokumento.message}`
            );
            commit(
              'setChangeDokumento',
              data.data.redaktuUniversoDokumento.universoDokumentoj
            );
          } else {
            console.log(
              `Документ не изменён. \nОшибка: ${
                data?.data?.redaktuUniversoDokumento?.message ?? 'Неизвестно'
              }`
            );
          }
        })
        .catch((error) => {
          console.error(error);
          this.dispatch('UIstore/hideLoading');
        });
    }
  }
};
