export function setConnected(state, payload) {
  state.connected = payload;
}

export function setNewMessage(state, payload) {
  state.newMessage = payload;
}
export function setNewMessageReceivers(state, payload) {
  state.newMessageReceivers = payload;
}
export function setNewMessageReceiver(state, payload) {
  const receiversSet = new Set(state.newMessageReceivers);
  if (receiversSet.has(payload)) {
    receiversSet.delete(payload);
  }
  state.newMessageReceivers = Array.from(receiversSet);
  if (state.newMessageReceivers.length === 0) {
    state.newMessage = null;
  }
}
