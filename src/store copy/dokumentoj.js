import {
  Dokumento,
  dokumentoKategorio,
  DokumentoTipo
} from 'src/queries/queries';
import { redaktuDokumento } from 'src/queries/mutations';
import { apollo } from 'src/boot/apollo';

export default {
  namespaced: true,
  state() {
    return {
      types: null,
      categories: null,
      statuses: null,
      edges: [],
      pageInfo: null,
      objId: null,
      needRefresh: false
    };
  },
  getters: {
    getDokumento: (state) => {
      // console.log(state);
      return {
        pageInfo: state.pageInfo,
        edges: state.edges.map((item) => item[1])
      };
    },
    getTipoj: (state) =>
      state.types ? state.types.universoDokumentoTipo : null,
    getKategorioj: (state) =>
      state.categories ? state.categories.universoDokumentoKategorio : null,
    getStatusoj: (state) => (state.statuses ? state.statuses.statuso : null)
  },

  mutations: {
    setDokumento: (state, payload) => {
      let mapList;
      if (state.needRefresh === false) {
        mapList = new Map(state.edges);
      } else {
        mapList = new Map();
      }

      payload.edges.forEach((item) => {
        mapList.set(item.node.uuid, item);
      });
      state.pageInfo = payload.pageInfo;

      state.edges = Array.from(mapList);
      state.needRefresh = false;
    },
    setNeedRefresh(state) {
      state.needRefresh = true;
      state.pageInfo = null;
    },
    setTipoj: (state, payload) => (state.types = payload),
    setKategorioj: (state, payload) => (state.categories = payload),
    setStatusoj: (state, payload) => (state.statuses = payload),
    setAddDokumento(state, payload) {
      state.edges.unshift([payload.uuid, { node: payload }]);
      // state.needRefresh = true;
      state.pageInfo = null;
    },
    setDelDokumento(state, payload) {
      const mapList = new Map(state.edges);
      if (mapList.has(payload)) {
        mapList.delete(payload);
      }
      state.edges = Array.from(mapList);
      // state.needRefresh = true;
      state.pageInfo = null;
    }
  },
  actions: {
    fetchDokumento({ commit }, payload) {
      // const {komunumoId} = payload;
      this.dispatch('UIstore/showLoading');
      return apollo.default
        .query({
          query: Dokumento,
          variables: payload,
          errorPolicy: 'all',
          fetchPolicy: 'network-only'
        })
        .then(({ data }) => {
          commit('setDokumento', data.universoDokumento);
          this.dispatch('UIstore/hideLoading');
        })
        .catch(() => {
          this.dispatch('UIstore/hideLoading');
        });
    },

    fetchKategorioj({ commit }) {
      this.dispatch('UIstore/showLoading');
      return apollo.default
        .query({
          query: dokumentoKategorio,
          errorPolicy: 'all',
          fetchPolicy: 'network-only'
        })
        .then(({ data }) => {
          commit('setKategorioj', data);
          this.dispatch('UIstore/hideLoading');
        })
        .catch(() => {
          this.dispatch('UIstore/hideLoading');
        });
    },
    // fetchStatusoj({commit}) {
    //   this.dispatch('UIstore/showLoading');
    //   return graphqlClient.query({
    //     query: universoProjektojProjektoStatusoQuery,
    //     errorPolicy: 'all', fetchPolicy: 'network-only',
    //
    //   }).then(({data}) => {
    //     commit('setStatusoj', data);
    //     this.dispatch('UIstore/hideLoading');
    //   }).catch(err => {
    //     this.dispatch('UIstore/hideLoading');
    //   });
    // },
    fetchTipoj({ commit }) {
      this.dispatch('UIstore/showLoading');
      return apollo.default
        .query({
          query: DokumentoTipo,
          errorPolicy: 'all',
          fetchPolicy: 'network-only'
        })
        .then(({ data }) => {
          commit('setTipoj', data);
          this.dispatch('UIstore/hideLoading');
        })
        .catch(() => {
          this.dispatch('UIstore/hideLoading');
        });
    },
    refetchDokumento({ commit }, payload) {
      this.dispatch('UIstore/showLoading');
      return apollo.default
        .query({
          query: Dokumento,
          variables: payload,
          errorPolicy: 'all',
          fetchPolicy: 'network-only'
        })
        .then(({ data }) => {
          commit('setDokumento', data);
          this.dispatch('UIstore/hideLoading');
        })
        .catch(() => {
          this.dispatch('UIstore/hideLoading');
        });
    },
    addDokumento({ commit }, payload) {
      this.dispatch('UIstore/showLoading');
      return apollo.default
        .mutate({
          mutation: redaktuDokumento,
          variables: payload,
          update: () => {
            // eslint-disable-next-line no-console
            console.log('Регистрируем документ');
          }
        })
        .then((data) => {
          this.dispatch('UIstore/hideLoading');
          if (data?.data?.redaktuUniversoDokumento?.status) {
            // eslint-disable-next-line no-console
            console.log(
              `Документ ${data.data.redaktuUniversoDokumento.universoDokumentoj?.uuid} успешно зарегистрирован.\nОтвет сервера: ${data.data.redaktuUniversoDokumento.message}`
            );
            commit(
              'setAddDokumento',
              data.data.redaktuUniversoDokumento.universoDokumentoj
            );
          } else {
            // eslint-disable-next-line no-console
            console.log(
              `Документ не зарегистрирован. \nОшибка: ${
                data?.data?.redaktuUniversoDokumento?.message ?? 'Неизвестно'
              }`
            );
          }
        })
        .catch((error) => {
          // eslint-disable-next-line no-console
          console.error(error);
          this.dispatch('UIstore/hideLoading');
        });
    },
    delDokumento({ commit }, payload) {
      const { uuid } = payload;
      this.dispatch('UIstore/showLoading');
      return apollo.default
        .mutate({
          mutation: redaktuDokumento,
          variables: payload,
          update: () => {
            // eslint-disable-next-line no-console
            console.log('Удаляем проект');
          }
        })
        .then((data) => {
          this.dispatch('UIstore/hideLoading');
          if (data?.data?.redaktuUniversoDokumento?.status) {
            // eslint-disable-next-line no-console
            console.log(
              `Документ ${uuid} успешно удален.\nОтвет сервера: ${data.data.redaktuUniversoDokumento.message}`
            );
            commit('setDelDokumento', uuid);
          } else {
            // eslint-disable-next-line no-console
            console.log(
              `Документ ${uuid} не удален. \nОшибка: ${
                data?.data?.redaktuUniversoDokumento?.message ?? 'Неизвестно'
              }`
            );
          }
        })
        .catch((error) => {
          this.dispatch('UIstore/hideLoading');
          // eslint-disable-next-line no-console
          console.error(error);
        });
    }
  }
};
